(ns experiment.command-centre
  (:require [active-class.problem :refer :all]
            [active-class.simulator :refer :all]
            [domains.graph-search :refer [make-graph-problem seq-centre]]
            [domains.grid-search-expt :as grid-search]
            [domains.grasping-expt :as grasping]
            [active-class.isolation-alg :refer [iso-alg]]
            [active-class.submodular-orienteering :refer [version-space-covering]]
            [experiment.runner :refer :all]
            [experiment.baseline :refer :all]
            )
  )



(defn graph_problem [n d]
  {:problem (make-graph-problem n d)
   :start seq-centre
   :epsilon 1.0
   })

(def grasping_problem
  {:problem grasping/problem
   :start 0
   :epsilon 1.0
   })

(def grid_problem
  {:problem grid-search/problem
   :start 10
   :epsilon 2.0
   })

(defn run-graphs-timing []
  (doseq [i [[6 53] [7 53] [8 53] [6 10] [5 10]]]
    (let [pb (apply make-graph-problem i)]
      (do (run-all greedy-loc pb seq-centre)
          (run-all greedy-distance pb seq-centre))))
  )

(defn run-graphs-alg-timing [alg]
  (doseq [i [ [6 53] [7 53] [8 53] [6 10] [5 10]  ]]
    (let [pb (apply make-graph-problem i)]
      (do (run-all alg pb seq-centre)(println i) )))
  )

(defn run-graphs-alg-parallel [alg]
  (doseq [i [[6 53] [7 53] [8 53] [6 10] [5 10]]]
    (let [pb (apply make-graph-problem i)]
      (do (run-allp  alg pb seq-centre)(println i) )))
  )

(defn run-timing [alg config]
  (binding [active-class.transform-tree/epsilon (:epsilon config)]
   (run-all alg (:problem config) (:start config))))

(defn run-parallel [alg config]
  (binding [active-class.transform-tree/epsilon (:epsilon config)]
   (run-allp alg (:problem config) (:start config))))
