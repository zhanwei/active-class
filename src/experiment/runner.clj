(ns experiment.runner
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [active-class.isolation-alg :refer [iso-alg]]
            [active-class.simulator :refer :all]
            [domains.grid-search :refer :all]
            [clojure.java.io :refer [writer]]
            )
  )

;; runalgorithm with all hypotheses in parallel

(defn run-many [alg problem start-loc hypotheses map-fn make-fn]
  (let [scenarios
        (map #(make-fn problem  % start-loc) hypotheses)
        results (map-fn (partial alg problem) scenarios)
        [correct wrong] (map (group-by #(= (:true-hypothesis (second %)) (first %)) results)
                             [true false])
        scores (map #(let [sim (second %)
                           th (:true-hypothesis sim) ]
                       (* (score sim) ((priors problem) th)))
                    correct)
        total-correct-prob (apply + (map #((priors problem) (:true-hypothesis (second %))) correct))
        avg-cost (/ (apply + scores ) total-correct-prob )

        delta-sq (map #(let [sim (second %)
                          th (:true-hypothesis sim) ]
                      (* (* (- avg-cost (score sim))(- avg-cost (score sim))) ((priors problem) th)))
                   correct) 
        variance (/ (apply + delta-sq ) total-correct-prob )

        avg-time (/ (apply + (map #(apply + (time-history (second %))) correct))
                    (count correct))
        avg-max-time (/ (apply + (map #(apply max 0 (time-history (second %))) correct))
                        (count correct))
        ]
    (do (println "average cost is " avg-cost)
        (println "average time is " avg-time)
        (println "variance is " variance)
        (println "average max time is " avg-max-time)
        (println "ratio correct" (/ (count correct) (count scenarios)))
        (if (seq wrong)
          (println "wrong cases: "(map #(:true-hypothesis (second %)) wrong)))
        avg-cost)
    ))

(defn run-allp [alg problem start-loc]
  (run-many alg problem start-loc (hypotheses problem) pmap
            make-noiseless-simulator))

(defn run-all [alg problem start-loc]
  (run-many alg problem start-loc (hypotheses problem) map
            make-noiseless-simulator))

(defn run-sample [alg problem start-loc num-samples]
  (let [hs (take num-samples (shuffle (hypotheses problem)))]
    (do
      (println hs)
      (run-many alg problem start-loc hs map))))

(defn run-once [alg problem start-loc hypothesis]
  (let [sim (make-noiseless-simulator problem hypothesis start-loc)
        _ (assert ((set (hypotheses problem)) hypothesis))
        result (time (alg problem sim))]
    (do (println (history (second result)))
        (println (time-history (second result)))
        (println "cost is " (score (second result)))
        (if (not= (first result) hypothesis)
          (do
            (println "wrong hypothesis got"
                     (first result))))
        (score (second result))))
  )
