(ns experiment.baseline
  (:require [active-class.problem :refer :all]
            [active-class.isolation-alg :refer [iso-alg]]
            [active-class.simulator :refer :all]
            [loom.alg :refer [dijkstra-path-dist]]
            [clojure.set :refer [difference]]
            [incanter.core :refer [log]]
            [clojure.math.combinatorics :refer [cartesian-product]]
            ))

;; calculate the information gain given a path
(defn info-gain [instance path]
  (if (and (seq path) (> (count (hypotheses instance)) 1))
    (let [h-priors (priors instance)
          obs-hs (group-by #(obs instance % (first path) nil)
                           (hypotheses instance))
          obs-next-instance (into {} (map (fn  [[o hs]]
                                            [o
                                             {:next (split instance hs)
                                              :prob (apply + (map h-priors hs))}])
                                          obs-hs))
          curr-entropy (entropy instance)
          ]
      (apply + (map (fn [[obs ni-prob]]
                      (* (:prob ni-prob)
                         (+ (- curr-entropy (entropy (:next ni-prob)))
                            (info-gain (:next ni-prob) (rest path)))))
                    obs-next-instance))
      )
    ;; else
    0
    )
  )

(def info-gain-memo (memoize info-gain))

(defn highest-ig-distance [instance curr-loc history]
  (let [remain-locs (difference (set (locations instance)) (set history))
        locs-gain (map #(vector % (/ (info-gain instance [%])
                              (get-in (metric instance) [curr-loc %])))
                       remain-locs)
        ;; _ (println (filter #(not (zero? (second %) )) locs-gain))
        next-loc (first  (apply max-key second (shuffle locs-gain)))
        ;; _ (println (hypotheses instance))
        ]
    next-loc)
  )

(defn highest-ig-loc [instance curr-loc history]
  (let [remain-locs (difference (set (locations instance)) (set history))
        locs-gain (map #(vector % (info-gain instance [%]))
                       remain-locs)
        ;;_ (println (filter #(not (zero? (second %) )) locs-gain))
        next-loc (first  (apply max-key second (shuffle locs-gain)))
        ;; _ (println (hypotheses instance))
        ]
    next-loc)
  )

;;  a k-step lookahead search
(defn highest-ig-path [instance curr-loc history k]
  (let [remain-locs (difference (set (locations instance)) (set history))
        all-paths (filter #(apply not= %)
                   (apply cartesian-product (repeat k remain-locs)))
        next-path (apply max-key #(/ (info-gain instance (vec %))
                                     (second (reduce (fn [[curr dist] next]
                                                [next
                                                 (+ dist (get-in (metric instance)
                                                                 [curr next]))]
                                                ) [curr-loc 0] %)))
                         all-paths
                        )
        ]
    (first next-path))
  )

(defn greedy-distance [instance simulator]
  (env-loop instance simulator highest-ig-distance))

(defn greedy-loc [instance simulator]
  (env-loop instance simulator highest-ig-loc))

(defn greedy-path-fn [k]
  (fn [instance simulator]
    (env-loop instance simulator
              (fn [instance curr-loc history]
                (highest-ig-path instance curr-loc history k)))))
