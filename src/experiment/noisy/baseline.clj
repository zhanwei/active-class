(ns experiment.noisy.baseline
  (:require [active-class.problem :refer :all]
            [active-class.noisy.simulator :refer :all]
            [active-class.simulator :refer [entropy]]
            [loom.alg :refer [dijkstra-path-dist]]
            [clojure.set :refer [difference]]
            ))

(defn info-gain [instance loc]
  (if (> (count (hypotheses instance)) 1)
    (let [h-priors (priors instance)
          obs-space (keys (obs-prob instance (first (hypotheses instance)) loc))
          obs-next-instance
          (into {} (map (fn  [o]
                          [o
                           {:next
                            (update-instance instance loc o)
                            :prob
                            (apply + (map #(* ((obs-prob instance (first %) loc) o)
                                              (second %))
                                          (priors instance)))
                            }])
                        obs-space))
          curr-entropy (entropy instance)
          ]
      (apply + (map (fn [[obs ni-prob]]
                      (* (:prob ni-prob)
                         (- curr-entropy (entropy (:next ni-prob)))
                         ))
                    obs-next-instance))
      )
    ;; else
    0
    )
  )

(def info-gain-memo (memoize info-gain))

(defn highest-ig-distance [instance curr-loc history _]
  (let [remain-locs (difference (set (locations instance)) (set history))
        locs-gain (map #(vector % (/ (info-gain instance %)
                              (get-in (metric instance) [curr-loc %])))
                       remain-locs)
        ;; _ (println (filter #(not (zero? (second %) )) locs-gain))
        next-loc (first  (apply max-key second (shuffle locs-gain)))
        ;; _ (println (hypotheses instance))
        ]
    next-loc)
  )

(defn highest-ig-loc [instance curr-loc history _]
  (let [remain-locs (difference (set (locations instance)) (set history))
        locs-gain (map #(vector % (info-gain instance %))
                       remain-locs)
        ;;_ (println (filter #(not (zero? (second %) )) locs-gain))
        next-loc (first  (apply max-key second (shuffle locs-gain)))
        ;; _ (println (hypotheses instance))
        ]
    next-loc)
  )

(defn greedy-distance [instance simulator]
  (env-loop instance simulator highest-ig-distance))

(defn greedy-loc [instance simulator]
  (env-loop instance simulator highest-ig-loc))
