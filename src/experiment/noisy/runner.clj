(ns experiment.noisy.runner
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [active-class.noisy.simulator :refer [make-noisy-simulator]]
            [active-class.simulator :refer :all]
            [experiment.runner :refer [run-many]]
            [clojure.java.io :refer [writer]]
            )
  )


(defn run-allp [alg problem start-loc]
  (run-many alg problem start-loc (hypotheses problem) pmap
            make-noisy-simulator))

(defn run-all [alg problem start-loc]
  (run-many alg problem start-loc (hypotheses problem) map
            make-noisy-simulator))

(defn run-sample [alg problem start-loc num-samples]
  (let [hs (take num-samples (shuffle (hypotheses problem)))]
    (do
      (println hs)
      (run-many alg problem start-loc hs map))))

(defn run-once [alg problem start-loc hypothesis]
  (let [sim (make-noisy-simulator problem hypothesis start-loc)
        _ (assert ((set (hypotheses problem)) hypothesis))
        result (time (alg problem sim))]
    (do (println (history (second result)))
        (println (time-history (second result)))
        (println "cost is " (score (second result)))
        (if (not= (first result) hypothesis)
          (do
            (println "wrong hypothesis got"
                     (first result))))
        (score (second result))))
  )
