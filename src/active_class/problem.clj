(ns active-class.problem
  (:require [loom.graph :refer :all])
  )

(defprotocol ActiveClass
  (hypotheses [this])
  (locations [this])
  (priors [this])   ;; returns a map of hypothesis to its probabilities
  (obs [this h loc rand])
  (obs-prob [this h loc])
  (metric [this])
  (metric-tree [this])
  (split [this new-h-set])
  )
