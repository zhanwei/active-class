(ns active-class.transform-tree
  (:require [metric-embedding.embed :refer :all]
            [clojure.set :refer [union]]
            [loom.graph :refer [successors edges nodes
                                subgraph
                                weighted-digraph
                                predecessors weight remove-edges add-edges]]
            [loom.alg :refer [post-traverse pre-traverse  dijkstra-path-dist]])
)

(def log2 (Math/log 2))

(def beta 10)

(def ^:dynamic epsilon 2.0)

(defn is-leaf? [tree node]
  (empty? (successors tree node)))


;; outputs a map of vertex to all leaves that are in the sub-tree
;; rooted at the vertex
(defn subtree-leaves [tree root]
  (reduce (fn [subtree-l node]
            (assoc subtree-l node
                   (if (zero? (count (successors tree node)))
                     (conj (apply union (map subtree-l (successors tree node))) node)
                     (apply union (map subtree-l (successors tree node))))))
          {}
          (post-traverse tree root))
  )





;; outputs an alpha decomposition of tree
(defn alpha-decompose [tree alpha root subtree-l]
  (let [threshold (/ (count (subtree-l root)) alpha)]
    (loop [heavy-nodes []
           stack [root]]
      (if-let [curr (peek stack)]
        (recur (conj heavy-nodes curr)
               (if (> (count (subtree-l curr)) threshold)
                 (into (pop stack) (successors tree curr))
                 (pop stack)))
        (subgraph tree heavy-nodes))
      ))
  )

(defn branch-promotion [tree root new-tree]
  (let [ ;; skeleton of tree, filter out the fluff
        sk-tree (subgraph tree (filter #(seq (successors tree %)) (nodes tree)))
        ]
    (loop [preorder (pre-traverse sk-tree root)
           curr-tree new-tree
           ]
      (if (seq preorder)
        (let [[curr-branch_ the-rest_] (split-with #(seq (successors sk-tree %)) preorder)
              curr-branch (if (seq the-rest_)
                            (conj (vec curr-branch_) (first the-rest_))
                            (vec curr-branch_))
              the-rest (rest the-rest_)
              branch-path-cost
              (reduce (fn [acc x]
                        (conj acc (+ (peek acc)
                                     (weight tree (first (predecessors sk-tree x)) x))))
                      [0]
                      (rest curr-branch))
              bunches
              (mapv #(inc (Math/floor (/ (Math/log %) log2))) branch-path-cost)
              v (first curr-branch)
              ]
          (recur
           the-rest
           (-> curr-tree
               ;; add the first part: from root to v
               ((fn [t] (if (not= v root)
                          (add-edges t [root v
                                        (second (dijkstra-path-dist tree root v))])
                          t)))
               ;; directly add leaves of v to v
               ((fn [t]
                  (reduce (fn [tt leaf]
                            (add-edges tt [v leaf (weight tree v leaf)]))
                          t
                          (filter #(empty? (successors tree %)) (successors tree v))
                          )))

               ;; add edges from v to non-empty bunches node Bi
               ((fn [t] (reduce (fn [tt k]
                                  (add-edges tt [v (str v "-" k) (Math/pow 2 k)]))
                                t
                                (distinct (rest bunches))))) ;;rest to remove bunch of v
               ;; add edges from Bi to leaves hanging from a vertex in Bi
               ((fn [t]
                  (reduce
                   (fn [tt idx]
                     (let [u (curr-branch idx)
                           b_node (str v "-" (bunches idx))
                           ]
                       (reduce (fn [ttt leaf]
                                 (add-edges ttt [b_node leaf (weight tree u leaf)]))
                               tt
                               (filter #(empty? (successors tree %)) (successors tree u))
                               )))
                   t
                   (range 1 (count curr-branch)))))
               )
           )
          )
        curr-tree)
      ))
  )



;; height reduction function
;; recursively do alpha-decomposition and branch promotion

(defn height-reduction
  ([tree root alpha new-tree subtree-l]
     (if (is-leaf? tree root)
       new-tree
       (let [Q (alpha-decompose tree alpha root subtree-l)
             new-t (reduce (fn [t leaf]
                             (height-reduction tree leaf alpha t subtree-l))
                           ;; do branch promotion for Q, adding result to the new tree
                           (branch-promotion Q root new-tree)
                           ;; iterate through the leaves of Q, the light nodes
                           (filter (partial is-leaf? Q) (nodes Q)))
             ]
         new-t)
       ))

  ([tree root alpha]
     (if (> alpha 1.0)
       (height-reduction tree root alpha (weighted-digraph) (subtree-leaves tree root))
       tree)
     )

  )


;; replace nodes of vector by integer
(defn replace-nodes [tree]
  (let [[virtual-nodes real-nodes] (map (group-by coll? (nodes tree))
                                        [true false])
        new-id-map (into {} (map vector virtual-nodes
                                 (iterate inc (inc (apply max real-nodes)))))
        ;;_ (println new-id-map)
        ]
    (apply add-edges (weighted-digraph)
           (for [e (edges tree)
                 :let [w (apply weight tree e)]]
             (conj (mapv #(if (coll? %) (new-id-map %) %) e)
                   w)))))

(defn degree-reduction
  ([tree root beta new-tree subtree-l]
     (if (is-leaf? tree root)
       new-tree
       (let [threshold (/ (count (subtree-l root)) beta)

             add-bunch
             (fn [curr-tree bunch]
               (-> curr-tree
                   (add-edges [root bunch 0])
                   ((fn [t] (apply add-edges t
                                   (for [c bunch]
                                     [bunch c (weight tree root c)])))
                    )))
             ]
         (reduce
          (fn [t c]
            (degree-reduction tree c beta t subtree-l))

          (loop [curr-tree new-tree
                 children (successors tree root)
                 acc 0
                 bunch []]
            (if-let [child (first children)]
              (cond
               ;; node is beta heavy, unmodified
               (>= (count (subtree-l child)) threshold)
               (recur (add-edges curr-tree [root child (weight tree root child)])
                      (rest children)
                      acc
                      bunch)

               ;; bunch is heavy with addition of this child, add bunch to root
               (>= (+ acc (count (subtree-l child))) threshold)
               (recur (add-bunch curr-tree (conj bunch child))
                      (rest children)
                      0
                      [])

               :else
               (recur curr-tree
                      (rest children)
                      (+ acc (count (subtree-l child)))
                      (conj bunch child))
               )
              ;;; add the last bunch if any
              (if (seq bunch)
                (add-bunch curr-tree bunch)
                curr-tree)
              )
            )
          (successors tree root)))))

  ([tree root beta]
     (replace-nodes
      (degree-reduction tree root beta (weighted-digraph) (subtree-leaves tree root))))

  )

(defn transform-tree [tree terminals root]
  (let [ ;; height and degree reduction
        alpha (max 1.0 (Math/pow (Math/log (count terminals)) epsilon))
        ]
    (-> tree
        (degree-reduction root beta)
        (height-reduction root alpha)
        )
    )
  )
