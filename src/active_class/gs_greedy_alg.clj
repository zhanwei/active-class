(ns active-class.gs-greedy-alg
  (:require [clojure.set :refer :all]
            [loom.graph :refer :all]
            [loom.alg :refer [pre-traverse post-traverse]]
            [tree.tree :refer :all]
            [clojure.math.numeric-tower :refer [ceil floor]]
            [active-class.group-steiner-tree :refer :all]
            [active-class.submodular-fn :refer :all]
            [active-class.group-fn :refer :all]
            )
  )

(defrecord TreeCover
    [nodes
     weight
     ])

(defn tree-density [submodular c]
  (let [total-profit (profits submodular (:nodes c))]
    (if (zero? total-profit)
      Double/MAX_VALUE
      (if (zero? (:weight c))
        ;; hack to make zero weight cover comparable
        ;; IMPORTANT: only works when maximum possible profits less than two
        (/ (* 2 Double/MIN_VALUE) total-profit)
        (/ (:weight c) total-profit)))))


;; outputs a map of vertex to all vertices that are in the sub-tree
;; rooted at the vertex
(defn subtree-descedants [tree root]
  (reduce (fn [subtree-desc node]
            (assoc subtree-desc node
                   (conj (apply union (map subtree-desc (successors tree node))) node)))
          {}
          (post-traverse tree root))
  )

(defn tree-height [tree root]
  (if (seq (successors tree root))
    (inc (apply max (map #(tree-height tree %) (successors tree root))))
    1))


;;wrapper function to the recursive one below
;; assumption: profits add up to one
(defn greedy-gs [tree submodular z root]
  (let [root-height (tree-height tree root)

        z_min (float (min-profit submodular))

        ;; map from node to vector of its descedants
        subtree-desc (subtree-descedants tree root)

        tree-edge-weight
        (fn [node]
          (if-let [pred (first (predecessors tree node))]
            (weight tree pred node)
            0)
          )

        lambda (/ 1 root-height)
        ]

    (letfn
      [(gen-search-candidates [root submodular z]
         (let [res-submodular (update-fn submodular [root])
               z_p (- z (profits submodular [root]))
               deg-root (count (successors tree root))
               start (/ z_p (* z_min deg-root (+ 1 (/ 1 lambda) (+ 1 lambda))))
               geo-series (->> (iterate (partial * (+ 1 lambda)) 1)
                               (drop-while (partial >= start))
                               (take-while (partial >= (/ z_p z_min)))
                               (map (partial * z_min)))

               children-max-profit
               (into {}
                     (for [child (successors tree root)]
                       [child (profits res-submodular (subtree-desc child))]))
               ;;_ (println z_p z root (successors tree root))
               ;;_ (println geo-series)

               children-min-profit
               (into {}
                     (for [child (successors tree root)]
                       [child
                        (apply min
                               Double/MAX_VALUE
                               (filter pos?
                                      (map #(profits res-submodular [%])
                                           (subtree-desc child)))
                               )]))

               ]
           (for [child (successors tree root)
                 z_pp geo-series
                 :when
                 (and (>= (children-max-profit child) (- z_pp 1e-7))
                      (or (<= (children-min-profit child) z_pp)(= z_pp (first geo-series)))
                      )]
             [child z_pp]
             )
           ))

         (augment-cover [curr aug]
           (let [new-nodes (difference (:nodes aug) (:nodes curr))
                 weight-delta (apply +  (map tree-edge-weight new-nodes))]
             (->TreeCover (union (:nodes curr) new-nodes)
                          (+ (:weight curr) weight-delta)
                          )))

         (tree-cover-from-node [node]
           (->TreeCover #{node} (tree-edge-weight node)))


         (get-subtree-child [root submodular child z height]
           (augment-cover
            (greedy-alg submodular z child (dec height) false)
            (tree-cover-from-node root)))

         (best-density-subtree
           [root submodular z height old-lower-bounds]
           ;; recurse for every u \in children (r') and every z'' \in [1,z_res]
           (let [init-lower-bound
                 (into
                  {}
                  (for [can (gen-search-candidates root submodular z)]
                    (if (nil? (old-lower-bounds can))
                      (let [[child z_pp] can
                            subtree (get-subtree-child root submodular child z_pp height)]
                        [can
                         {:val (tree-density submodular subtree)
                          :subtree subtree
                          :updated true}])
                      [can (assoc (old-lower-bounds can) :updated false)])))
                 ]
             (if (seq init-lower-bound)
               (loop [lb init-lower-bound]
                 (let [best-key-lb (apply min-key #(:val (second %)) lb)
                       best-key (first best-key-lb)
                       ]
                   (if (:updated (lb best-key))
                     [lb (:subtree (lb best-key))]
                     (let [[child z_pp] best-key
                           updated-subtree (get-subtree-child root submodular child z_pp height)]
                       (recur
                         (assoc lb best-key
                                {:val (tree-density submodular updated-subtree)
                                 :subtree updated-subtree
                                 :updated true})
                         ))
                     )
                   )
                 )
                [{} (tree-cover-from-node root)]
               )))

         (greedy-alg
           [submodular z root height top]
           (if (empty? (successors tree root))
             ;; return new cover
             (tree-cover-from-node root)
             ;; else
             (let [min-density
                   (fn [cover cover-h]
                     (if (or (nil? cover-h))
                       cover
                       (min-key (partial tree-density submodular) cover cover-h)
                       ))]
               (loop [cover (->TreeCover #{} 0 )
                      cover-h nil
                      z_res z
                      res-submodular submodular
                      lower-bounds {}]
                 (if (>= z_res 1e-7)
                   (let [[new-lb t_aug]
                         (best-density-subtree root
                                               res-submodular
                                               z_res
                                               height
                                               lower-bounds)
                         curr-profits (profits res-submodular (:nodes t_aug))
                         new-cover (augment-cover cover t_aug)
                         ;;_ (println z curr-profits)
                         ;;_ (assert (>= curr-profits z_min ) (prn-str  z_res root t_aug cover))
                         _ (if top (do
                                     (assert (or (> curr-profits 0)(> (- z z_res) 0))
                                             (prn-str z cover lower-bounds))
                                     (comment (if (not (or (> curr-profits 0)(> (- z z_res) 0)))
                                                (throw (Exception. (str  tree z cover lower-bounds)))))
                                     (println curr-profits)
                                     ))
                         ]
                     (if (> curr-profits 0)
                       (recur new-cover
                              ;; check the previous cover
                              (if (nil? cover-h)
                                (if (>= (+ (- z z_res) curr-profits) (/ z (inc height)))
                                  new-cover
                                  nil)
                                cover-h)
                              (- z_res curr-profits)
                              (update-fn res-submodular (:nodes t_aug))
                              new-lb
                              )
                       (min-density cover cover-h)
                       )
                     )
                   (do
                     (comment (if (and top
                              (= cover-h (min-density cover cover-h))
                              (< (profits submodular (:nodes cover-h)) (- z 1e-7)))
                       (do (println "recursing")
                         (augment-cover
                           cover-h
                           (greedy-alg (update-fn submodular (:nodes cover-h))
                                       (- z (profits submodular (:nodes cover-h)))
                                       root
                                       height
                                       true
                                       ))
                         )
                       (min-density cover cover-h)))
                     (min-density cover cover-h)
                     ))))))
         ]
     (trampoline greedy-alg submodular z root root-height true))))
