(ns active-class.version-sp-fn
  (:require [active-class.submodular-fn :refer :all]
            [active-class.problem :refer :all]
            [clojure.set :refer [union intersection difference]]
            ))

(defn version-space [sfun h locs]
  (let [all-locs (union (set locs) (:visited sfun))]
    (if (contains? @(:cache sfun) [h all-locs])
      (do (println "cache hit") (@(:cache sfun) [h all-locs]))
      (let [h-obs (map  #((:obs-model-fn sfun) [h %])
                        (difference (set locs) (:visited sfun)))
            vs (reduce (fn [consistent-set loc-obs]
                         (intersection ((:obs-h-set sfun) loc-obs) consistent-set))
                       ((:h-vs-covered sfun) h)
                       (map vector locs h-obs)
                       )
            _ (swap! (:cache sfun) assoc [h all-locs] vs)]
        vs
        ))))


(defrecord VersionSpaceFn
    [;; prior probabilities of hypotheses
     priors
     ;; probabilities of observation at a given location given prior
     obs-model-fn
     ;; mapping from location and observation to set of consistent hypotheses
     obs-h-set
     ;; locations visited
     visited
     ;; version space covered
     h-vs-covered
     ;; locations available
     valid-locs
     ;; expected mass of version space covered
     covered
     cache
     ]
  SubmodularFn
  (profits [this locs]
    (let [locs_ (filter #(contains? valid-locs %) locs) ]
      (if (seq locs_)
        (- 1.0
           (reduce (fn [pft [h h-prob]]
                     (+ pft (* (- (apply + (map priors (version-space this h locs_)))
                                  h-prob)
                               h-prob)))
                   0 priors)
           covered)
        0
        ))
    )

  (update-fn [this locs]
    (let [locs_ (filter #(and (contains? valid-locs %)
                              (not (contains? visited %))) locs)]
      (->  this
           (assoc :visited (union (set locs_) visited))
           (assoc :covered (+ covered (profits this locs_)))
           (assoc :h-vs-covered
             (reduce (fn [h-vs h]
                       (update-in h-vs [h] (partial intersection
                                                    (version-space this h locs_))))
                     h-vs-covered
                     (keys priors))
             )
           ))
    )

  (min-profit [this]
    (do (comment (let [vl (vec valid-locs)
                       pl (mapv #(profits this [%]) vl)]
                   (apply min (filter #(> % 1e-5) (map (fn [[p1 p2]] (Math/abs
                                                                      (- p1 p2)))
                                                       (for [i (range (count valid-locs))
                                                             j (range i)]
                                                         [(pl i) (pl j)])
                                                       )))))
        (apply min (filter pos? (map #(profits this [%]) valid-locs))))
    )

  )

(defn make-version-sp-fn [myproblem]
  (->VersionSpaceFn
   (priors myproblem)
   (into {} (for [h (hypotheses myproblem)
                  loc (locations myproblem)]
              [[h loc] (obs myproblem h loc nil) ]))
   (apply merge-with union (for [l (locations myproblem)
                                 h (hypotheses myproblem)]
                             {[l (obs myproblem h l nil)] #{h}}))
   #{}
   (into {} (map #(vector % (set (hypotheses myproblem)))
                 (hypotheses myproblem)))
   (set (locations myproblem))
   0
   (atom {})
   )
  )
