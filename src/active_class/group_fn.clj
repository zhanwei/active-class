(ns active-class.group-fn
  (:require [active-class.submodular-fn :refer :all]
            [clojure.set :refer :all]
))

(defn remove-groups [grp-profit groups-remove]
  (reduce (fn [pft group]
            (assoc pft group 0)) grp-profit groups-remove)
  )

(defn groups-covered-node [nodes-groups node]
  (get nodes-groups node #{})
  )

(defn groups-covered [nodes-groups items]
  (reduce (fn [gg n]
            (union gg (groups-covered-node nodes-groups n)))
          #{} items))

(defn profits-covered [nodes-groups grp-profit items]
  (apply + (map grp-profit (groups-covered nodes-groups items))))

(defrecord GroupFn
  [grp-profit
   groups
   nodes-groups]
  SubmodularFn
  (update-fn  [this items]
    (assoc this :grp-profit
           (remove-groups grp-profit
                          (groups-covered nodes-groups items))))

  (profits [this items] (profits-covered nodes-groups grp-profit items))

  (min-profit [this]
    (apply min (vals grp-profit)))
)

(defn make-group-fn [profits groups]
  (->GroupFn profits
             groups
             (apply merge-with union (for [g groups x (second g)]
                                  {x #{(first g)}}))))
