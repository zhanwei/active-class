(ns active-class.noisy.isolation-noisy
  (:require [active-class.problem :refer :all]
            [clojure.set :refer [difference union]]
            [active-class.noisy.simulator :refer :all]
            [active-class.isolation-alg :refer [partition-alg]]
            )
)

(defrecord DetermineModel
    [model
     scenarios  ;; a tuple of hypotheses and random number map, one for
                ;; each location
     scenario-priors
     remaining-locs
     ]
  ActiveClass
  (hypotheses [this] scenarios)

  (metric [this] (metric model))

  (metric-tree [this] (metric-tree model))

  (locations [this] remaining-locs)

  (obs-prob [this scenario loc]
    (into {} (map #(if (= (first %) (obs this scenario loc nil))
                     [(first %) 1.0]
                     [(first %) 0.0])
                  (obs-prob model (first scenario) loc))))

  (obs [this scenario loc _]
    (let [h (first scenario)
          rand-number ((second scenario) loc)]
      (obs model h loc rand-number)
      ))

  (split [this new-h-set]
    (do (assert (seq new-h-set) "empty hypothesis set")
        (assoc this :scenario-priors
            (let [total-prob (apply + (vals (select-keys scenario-priors new-h-set)))]
              (reduce #(conj %1 [%2 (/ (scenario-priors %2) total-prob)])
                      {}
                      new-h-set)
              ))))

  (priors [this] scenario-priors)
  )

(defn make-deterministic-model [instance num-copies remaining-locs]
  (let [scenarios
        (->> (hypotheses instance)
             (filter #(> ((priors instance) %) 1e-3))
             (map
                (fn [h]
                  (map vector
                       (repeat num-copies h)
                       (repeatedly num-copies
                                   #(into {} (for [l remaining-locs]
                                               [l (rand)])))
                       ))
                )
             (apply concat)
             )
        scenario-priors
        (into {}
              (for [s scenarios]
                [s (/ ((priors instance) (first s))
                      num-copies)]))
        ]
    (->DetermineModel instance scenarios scenario-priors
                      remaining-locs)
    )
  )


(defrecord MLModel
    [model
     remaining-locs
     h-priors
     ]
  ActiveClass
  (hypotheses [this] (set (keys h-priors)))

  (metric [this] (metric model))

  (metric-tree [this] (metric-tree model))

  (locations [this] remaining-locs)

  (obs-prob [this h loc]
    { (first (max-key second (obs-prob model h loc)))
      1.0}

    )

  (obs [this h loc _]
    (first (max-key second (obs-prob model h loc)))
    )

  (split [this new-h-set]
    (assoc this :h-priors
           (let [total-prob (apply + (vals (select-keys h-priors new-h-set)))]
             (reduce #(conj %1 [%2 (/ (h-priors %2) total-prob)])
                     {}
                     new-h-set)
             )))

  (priors [this] h-priors)
  )


(defn make-ml-model [instance remaining-locs]
  (->MLModel instance remaining-locs (priors instance)))


(defn iso-noisy-policy [instance num-copies]
  (let [path (atom nil)]
    (fn [instance curr-loc history obs]
      (do
        ;;(println @path obs curr-loc history)
        (if (or (<= (count @path) 1) (not= obs ((first @path) 2)))
         ;; replace
         (let [determine-instance (make-deterministic-model
                                   instance
                                   num-copies
                                   (clojure.set/difference (locations instance)
                                                           history))
               [tau _] (partition-alg determine-instance curr-loc)
               [next-loc small-keys] (first tau)
               ]
           (do (swap! path (fn [_] tau))
               next-loc)
           )
         (do (swap! path (fn [p] (rest p)))
             (ffirst @path))
         ))
      ))
  )


;; IN  function to make some transformed instance
;; make-instance-fn takes 2 parameters: original instance and set remaining locations
(defn iso-noisy-policy [make-instance-fn]
  (let [path (atom nil)]
    (fn [instance curr-loc history obs]
      (do
        ;;(println @path obs curr-loc history)
        (if (or (<= (count @path) 1) (not= obs ((first @path) 2)))
          ;; replace
          (let [transformed-instance (make-instance-fn
                                instance
                                (clojure.set/difference (locations instance)
                                                        history))
                [tau _] (partition-alg transformed-instance curr-loc)
                [next-loc small-keys] (first tau)
                ]
            (do (swap! path (fn [_] tau))
                next-loc)
            )
          (do (swap! path (fn [p] (rest p)))
              (ffirst @path))
          ))
      ))
  )

(def ^:dynamic num-copies 5)

(defn iso-deterministic-alg [instance simulator]
  (env-loop instance simulator (iso-noisy-policy
                                #(make-deterministic-model %1 num-copies %2))))

(defn iso-ml-alg [instance simulator]
  (env-loop instance simulator (iso-noisy-policy make-ml-model)))
