(ns active-class.noisy.edge-cutting-isolation
  (:require [active-class.problem :refer :all]
            [active-class.noisy.simulator :refer :all]
            [active-class.noisy.isolation-noisy :refer [iso-noisy-policy]]
            )
)

(defrecord ECModel
    [model
     edge-priors
     remaining-locs
     ]
  ActiveClass
  (hypotheses [this] (set (keys edge-priors)))

  (metric [this] (metric model))

  (metric-tree [this] (metric-tree model))

  (locations [this] remaining-locs)

  ;; what is this?
  (obs-prob [this edge loc]
    {(obs this edge loc nil) 1.0})

  (obs [this edge loc _]
    (let [obss (map #((second %) loc) edge)]
      (if (apply = obss)
        (first obss)
        :inconsistent)))

  (split [this new-edge-set]
    (assoc this :edges-priors
           (let [total-prob (apply + (vals (select-keys edge-priors new-edge-set)))]
             (reduce #(conj %1 [%2 (/ (edge-priors %2) total-prob)])
                     {}
                     new-edge-set)
             )))

  (priors [this] edge-priors)
  )

(defn make-hypothesis-sampler [hypotheses this-priors]
  (let [hypotheses-vec (vec hypotheses)
        cum-priors (reduce (fn [acc h]
                             (if (empty? acc)
                               (conj acc (this-priors h))
                               (conj acc (+ (peek acc) (this-priors h)))))
                           []
                           hypotheses-vec
                           )
        binary-search
        (fn [x cum-prob]
          (loop [l 0 h (unchecked-dec (count hypotheses-vec))]
            (if (<= h (inc l))
              (if (<= x (cum-prob l)) l h)
              (let [m (unchecked-add l (bit-shift-right (unchecked-subtract h l) 1))]
                (if (< (cum-prob m) x)
                  (recur (unchecked-inc m) h)
                  (recur l m))))))
        ]
    ;; sampling without replacement
    (fn [rand-float1 rand-float2]
      (let [;; sample two indices without replacement
            h1 (binary-search rand-float1 cum-priors)
            hyp1 (hypotheses-vec h1)
            ;; simulate deleting h1 from hypotheses-vec cum-priors
            r2 (* (- 1.0 (this-priors hyp1)) rand-float2)
            cum2 (fn [i] (if (> i h1)
                           (- (cum-priors i) (this-priors hyp1))
                           (cum-priors i)))
            h2_ (binary-search r2 cum2)
            h2 (if (= h1 h2_) (mod (inc h2_) (count hypotheses-vec)) h2_)
            _ (println h1 h2_)
            _ (assert (not= h1 h2) "sampled the same hypothesis")
            ]
        (map hypotheses-vec [h1 h2]))
      )
    )
  )

(defn log-obs-vec-prob [obs-vec instance h]
  (apply +
         (Math/log ((priors instance) h))
         (map (fn [[l o]]
                  (Math/log ((obs-prob instance h l) o))) obs-vec)
         )
  )

(defn make-edge-cutting-model [num-samples instance remaining-locs]
  (let [normalize (fn [priors] (let [total-prob (apply + priors)]
                                 (mapv #(/ % total-prob) priors)))
        _ (println (priors instance))
        sampler (make-hypothesis-sampler (hypotheses instance) (priors instance))
        edges (loop [es []]
                (if (< (count es) num-samples)
                  (let [[h1 h2] (sampler (rand) (rand))
                        edge (map
                              (fn [h]
                                [h (into {} (for [l remaining-locs]
                                              [l (obs instance h l (rand))]))]
                                )
                              [h1 h2])]
                    (if (apply = (map second edge))
                      (do (println "this is impossible")
                          (println (map second edge))
                          (println (map first edge))
                          (recur es))
                      (recur (conj es edge))))
                  es))
        priors (map #(Math/exp
                      (apply + (map
                                (fn [h]
                                  (log-obs-vec-prob (second h) instance (first h)))
                                %)
                             ))
                    edges)
        norm-priors (-> priors
                        normalize
                        ((fn [pp]
                           (let [h2 (Math/pow (/ 1.0 (count pp)) 2)]
                             (mapv #(if (< % h2) h2 %)  pp))))
                        normalize)
        edge-priors (into {} (map vector edges norm-priors))
        _ (println (apply min norm-priors))
        _ (println (count norm-priors))
        ]
    (->ECModel instance edge-priors remaining-locs)
    )
  )


(defn get-iso-ec-alg [num-edges-multi]
 (fn [instance simulator]
  (env-loop instance simulator (iso-noisy-policy
                                (partial make-edge-cutting-model
                                         (* num-edges-multi
                                            (count (priors instance)))) ))) )
