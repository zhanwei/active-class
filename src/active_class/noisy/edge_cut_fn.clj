(ns active-class.noisy.edge-cut-fn
  (:require [active-class.submodular-fn :refer :all]
            [active-class.problem :refer :all]
            [active-class.noisy.simulator :refer [update-instance]]
            [clojure.set :refer :all]
            ))


(defn os-max-probs-h [instance o_max_prob locs h]
  (apply * (map #(let [o_max_l (first (o_max_prob %))]
                   ((obs-prob instance h %) o_max_l))
                locs))
  )

(def os-max-probs-h-mem (memoize os-max-probs-h))

(defn compute-o-max-prob [instance locs]
  (into {}
        (for [l locs]
          [l (apply max-key second
                    (apply merge-with + (for [[h h-prob] (priors instance)
                                              [o o-prob] (obs-prob instance h l)]
                                          {o (* o-prob h-prob)}
                                          )))]))
  )

(defn weight-remain [instance o_max_prob locs prob-observed prob-observed-h]
  (let [os_max_prob (apply * (map #(second (o_max_prob %)) locs))]
    (apply
     -
     (Math/pow (* prob-observed os_max_prob) 2)
     (map (fn [[h prob]]
            (Math/pow
             (* prob
                (os-max-probs-h-mem instance o_max_prob locs h)
                (prob-observed-h h))
             2)
            )
          (priors instance))))
  )

(def weight-remain-mem (memoize weight-remain))


(defn weight-cut [instance o_max_prob locs prob-observed prob-observed-h
                  total-profit]
  (if (seq locs)
    (- total-profit
       (weight-remain-mem instance o_max_prob locs prob-observed prob-observed-h))
    0.0
    )
  )


(defrecord EdgeCutFn
    [instance
     valid-locs
     total-profit
     prob-observed ;; prob of o_max observed at previous locations
     prob-observed-h ;; prob of o_max observed at previous locations given h
     o_max_prob ;; observation witn max likelihood and corresponding prob
     ]
  SubmodularFn
  (update-fn [this items]
    (let [locs (filter #(contains? valid-locs %) items)]
      (if (seq locs)
        (-> this
            (assoc :valid-locs (difference valid-locs (set locs)))
            (assoc :total-profit (weight-remain-mem instance o_max_prob locs
                                                    prob-observed
                                                    prob-observed-h))
            (assoc :prob-observed (* prob-observed
                                     (apply * (map #(second (o_max_prob %)) locs))))
            (assoc :prob-observed-h
              (into {} (map (fn [[h prob]]
                              [h (* prob
                                    (os-max-probs-h instance o_max_prob locs h))])
                            prob-observed-h)))
            )
        this)) )

  (profits [this items]
    (let [locs (filter valid-locs items)]
      (weight-cut instance o_max_prob locs prob-observed prob-observed-h total-profit))
    )
  (min-profit [this]
    (let [x  (apply min-key second (map #(vector % (profits this [%])) valid-locs))
          _ (println "min profit" x)
          _ (assert (> (second x) 0.0) (prn-str (priors instance) valid-locs))]
      (second x))
    )
  )

(defn update-other-obs [ec-fn loc obs]
  (let [instance (:instance ec-fn)
        obs-prob-h (into
                    {}
                    (map (fn [[h prob]]
                           [h (* prob ((obs-prob instance h loc) obs))])
                         (:prob-observed-h ec-fn)))
        obs-prob (apply + (map #(* (second %)
                                   ((priors instance) (first %)))
                               obs-prob-h))]
    (-> ec-fn
        (assoc :valid-locs (disj (:valid-locs ec-fn) loc))
        (assoc :total-profit (apply -
                                    (Math/pow (* (:prob-observed ec-fn) obs-prob) 2)
                                    (map (fn [[h prob]]
                                           (Math/pow
                                            (* prob
                                               (obs-prob-h h))
                                            2)
                                           )
                                         (priors instance))))
        (assoc :prob-observed (* (:prob-observed ec-fn)
                                 obs-prob))
        (assoc :prob-observed-h obs-prob-h)
        ))
  )

(defn make-edge-cut-fn [instance valid-locs]
  (let [total-profit (- 1.0 (apply + (map #(* (second %) (second %)) (priors instance))))
        prob-observed 1.0
        prob-observed-h (into {} (map #(vector % 1.0) (hypotheses instance)))
        o_max_prob (compute-o-max-prob instance (locations instance))
        nonzero-profit-locs
        (filter #(> (weight-cut instance o_max_prob [%] prob-observed
                                prob-observed-h total-profit)
                    1e-7) valid-locs)]

    (->EdgeCutFn
     instance
     (set nonzero-profit-locs)
     total-profit
     prob-observed
     prob-observed-h
     o_max_prob
     ))
  )
