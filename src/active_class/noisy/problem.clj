(ns active-class.noisy.problem
  )

(defprotocol ActiveClassNoisy
  (hypotheses [this])
  (locations [this])
  (priors [this])   ;; returns a map of hypothesis to its probabilities
  (obs [this h loc rand])
  (metric [this])
  (metric-tree [this])
  (obs-prob [this h loc])
  (split [this new-h-set])
  )
