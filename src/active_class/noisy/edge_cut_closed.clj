(ns active-class.noisy.edge-cut-closed
  (:require [active-class.problem :refer :all]
            [clojure.set :refer [difference union]]
            [active-class.noisy.simulator :refer :all]
            [active-class.gs-greedy-alg :refer [greedy-gs]]
            [active-class.isolation-alg :refer [tsp tsp-nn rotate-tree]]
            [active-class.transform-tree :refer [transform-tree]]
            [active-class.noisy.edge-cut-fn :refer [make-edge-cut-fn]]
            [active-class.submodular-fn :refer :all]
            )
)


(defn ec-orienteering [instance start-loc visited]
  (let [dist-mat (metric instance)
        terminals (difference (set (locations instance)) visited)
        edge-cut-fun (make-edge-cut-fn instance terminals)
        _ (println "target is " (* 0.5 (:total-profit edge-cut-fun)))
        _ (println visited)
        ;;target (max 0.5 (+ (profits sfun [start-loc]) (min-profit sfun)))
        cover (->
               (rotate-tree (metric-tree instance) start-loc)
               (transform-tree terminals start-loc)
               (greedy-gs edge-cut-fun (* 0.5 (:total-profit edge-cut-fun)) start-loc ))
        tour (tsp-nn dist-mat start-loc (filter #(contains? terminals %) (:nodes cover)))
        _ (println tour)
        _ (if (empty? tour) (throw (Exception. "empty tour")))
        ]
    (map #(vector % (first (:o_max_prob %))) tour)
    ))

(defn edge-cutting-iso-policy [instance start-loc visited]
  (let [path (atom nil)]
    (fn [instance curr-loc history obs]
      (do
        ;;(println @path obs curr-loc history)
        (if (or (<= (count @path) 1) (not= obs (second (first @path))))
         ;; replace
          (let [tau (ec-orienteering instance curr-loc history)
                [next-loc big-key] (first tau)
               ]
           (do (swap! path (fn [_] tau))
               next-loc)
           )
         (do (swap! path (fn [p] (rest p)))
             (ffirst @path))
         ))
      ))
  )

(defn iso-ec-closed-alg [instance simulator]
  (env-loop instance simulator (edge-cutting-iso-policy
                                instance
                                (:loc simulator)
                                (:hist simulator))))
