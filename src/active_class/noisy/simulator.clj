(ns active-class.noisy.simulator
  (:require [active-class.problem :refer :all]
            [active-class.simulator :refer [ActiveClassSimulator
                                            location
                                            observation
                                            goto
                                            history
                                            time-history]]
            )
)

(defrecord NoisySimulator
    [true-hypothesis
     loc
     distance-total
     instance
     hist
     time-hist
     ]

  ActiveClassSimulator
  (observation [this randnum]
    (obs instance true-hypothesis loc randnum))

  (location [this] loc)

  (goto [this next-loc compute-time] (-> this
                            (assoc :loc next-loc)
                            (assoc :distance-total
                              (+ distance-total
                                 (get-in (metric instance) [loc next-loc] )))
                            (update-in [:hist] conj next-loc)
                            (update-in [:time-hist] conj compute-time)
                            ))
  (score [this] distance-total)
  (history [this] hist)
  (time-history [this] time-hist)
  )

(defn make-noisy-simulator [instance h start-loc]
  (->NoisySimulator h start-loc 0.0 instance [start-loc] []))

(defn update-instance [instance loc o]
  (let [posterior (into {} (map (fn [[h prob]]
                                   [h (* prob ((obs-prob instance h loc) o))]
                                   )
                                 (priors instance)))
        ]
    (split instance
           (let [;; remove low probability hypotheses
                 new-priors (into {} (filter #(> (second %) 1e-7) posterior))
                 total-prob (apply + (vals new-priors))
                 normed-priors
                 (into {} (map #(vector % (/ (new-priors %) total-prob))
                               (keys new-priors))
                       )
                 ]
             normed-priors
             ))))

(defn env-loop [instance_ simulator_ policy]
  (try
    (loop [instance instance_
           simulator simulator_
           ]
      (let [curr-loc (location simulator)
            obs (observation simulator (rand))
            new-instance (update-instance instance (location simulator) obs)
            _ (println (apply max (map second (priors new-instance))))
            ]
        (cond (>=  (apply max (map second (priors new-instance))) 0.9)
              [(first (apply max-key second (priors new-instance))) simulator]
              (>= (count (set (history simulator)))
                  (count (locations instance)))
              [nil simulator]
              :else
              (let [start (System/nanoTime)
                    next-loc (policy new-instance curr-loc (history simulator) obs)
                    end (System/nanoTime)
                    ]
                (recur new-instance (goto simulator
                                          next-loc
                                          (/ (- end start)
                                             1000000.0))))
              )
        )
      )
    (catch Exception e (println "Hypothesis: " (:true-hypothesis simulator_) (.getMessage e)))))
