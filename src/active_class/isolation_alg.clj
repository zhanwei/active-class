(ns active-class.isolation-alg
  (:require [active-class.problem :refer :all]
            [clojure.set :refer [difference union]]
            [active-class.group-steiner-tree :refer :all]
            [active-class.simulator :refer :all]
            [active-class.gs-greedy-alg :refer [greedy-gs]]
            ;; [metric-embedding.embed :refer :all]
            [jordanlewis.data.union-find :as uf]
            [active-class.double-list :refer :all]
            [active-class.group-fn :refer :all]
            [loom.graph :refer [successors edges nodes
                                weighted-digraph
                                predecessors weight remove-edges add-edges]]
            [loom.alg :refer [post-traverse]])
)

(def epsilon 3.0)

(defn partition-by-loc [instance loc]
  (group-by #(obs instance % loc nil) (hypotheses instance)))

(defn big-small-sets [instance partitions]
  (let [threshold (/ (count (hypotheses instance)) 2)
        tf-sets (group-by
                 #(and (> (apply + (map (priors instance) (second %))) 0.5)
                       (not= :inconsistent (second %)))
                 ;; #(> (count (second %)) threshold)
                 partitions)
        big-set (mapcat second (tf-sets true))
        small-set (mapcat second (tf-sets false))
        small-keys (keys (tf-sets false))
        big-key (keys (tf-sets true))]
    [big-set small-set small-keys (first big-key)]
    )
  )

;; given a path (r,x_1,x_2,..)and a map from location to Dx, complement Dx and H_o,x
;; outputs a vector of maps of observation to partition P_o,x (location implicit by ordering)

(defn partition-by-path [instance path loc-tuples]
  (let [;; Dx-s accumulated over the path
        D-acc (reduce
               (fn [Ds x]
                 (conj Ds (union (set (get-in loc-tuples [x :Dx]))
                                 (peek Ds))))
               [ #{}] ;;offset by one
               path)]

    (conj (mapv (fn [Da x]
                   (reduce-kv (fn [parts o part]
                                (conj parts {o (difference (set part) Da)}))
                              {}
                              (get-in loc-tuples [x :partitions])))
                 D-acc
                 path)
          ;; P_t
          (difference (set (hypotheses instance)) (peek D-acc))
          )
    )
  )

;; rotate a tree so that "root" is its root of the tree
(defn rotate-tree [tree root]
  (loop [curr-tree tree
         curr-node root]
    (if (empty? (predecessors tree curr-node))
      curr-tree
      (let [pred (first (predecessors tree curr-node))
            w (weight curr-tree pred curr-node)]
        (recur (-> curr-tree
                   (remove-edges [pred curr-node])
                   (add-edges [curr-node pred w])
                   )
               pred))))
  )

(defn- kruskal-mst [graph]
  (loop [sets (apply uf/union-find (nodes graph))
         tree #{}
         es (sort #(< (apply weight graph %1)
                      (apply weight graph %2)) (edges graph))
         ]
    (if-let [[u v] (first es)]
      (let [[new-sets cu] (uf/get-canonical sets u)
            [new-sets2 cv] (uf/get-canonical new-sets v)]
        (if (not= cu cv)
          (recur (uf/union new-sets2 cu cv) (conj tree [u v]) (rest es))
          (recur new-sets2 tree (rest es))))
      tree)
    )
  )

(defn- make-eulerian-graph [graph edges]
  (reduce (fn [g edge] (let [w (apply weight graph edge)]
                         (add-edges g
                                    (conj edge w)
                                    (conj (vec (reverse edge)) w))))
          (weighted-digraph)
          edges))

(defn- euler-circuit [graph root]
  (loop [tour (double-list [root])
         unused-edges
         (reduce (fn [m node]
                   (conj m [node (set (successors graph node))]))
                 {}
                 (nodes graph))
         start :start
         v root
         tour-pos 0
         ]
    (if (seq unused-edges)
      (if (= start v)
        ;; go to next vertex with ununsed edge
        (let [[pos new-start]
              (some #(when (seq (unused-edges (second %))) %)
                    (map vector (range) (seq tour)))
              ]
          (recur tour unused-edges :start new-start pos)
          )
        (let [u (first (unused-edges v))]
          (recur (add-after tour (get-nth tour tour-pos) u)
                 (let [el (disj (unused-edges v) u)]
                   (if (seq el)
                     (assoc unused-edges v el)
                     (dissoc unused-edges v)))
                 (if (= start :start)
                   v
                   start)
                 u
                 (inc tour-pos))))
      (seq tour)
      )))

(defn tsp [dist-mat root cover]
  (let [nodes (set (conj cover root))
        cgraph (apply add-edges
                      (weighted-digraph)
                      (for [start nodes
                            end nodes
                            :when (not= start end)]
                        [start end (get-in dist-mat [start end])]))
        mst-edges (kruskal-mst cgraph)
        egraph (make-eulerian-graph cgraph mst-edges)
        e-tour (euler-circuit egraph root)
        ]
    ;; returns the hamiltonian tour
    (loop [tour e-tour
                                node-visited #{}
                                h-tour []]
                           (if-let [n (first tour)]
                             (recur (rest tour) (conj  node-visited n)
                                    (if (contains? node-visited n)
                                      h-tour
                                      (conj h-tour n)))
                             h-tour)
                           )
    ))

(defn tsp-nn [dist-mat root nodes]
  (loop [ns (disj (set nodes) root)
         curr root
         tour []
         ]
    (if (empty? ns)
      tour
      (let [;;nn (apply min-key #(/ (get-in dist-mat [curr %])
            ;;                      (nodes-profit %)) ns)
            nn (apply min-key #(get-in dist-mat [curr %]) ns)
            ]
        (recur (disj ns nn) nn (conj tour nn))))
    ))

(defn gs-orienteering [instance groups profits root]
  ;;; convert to tree metric
  ;;; call max-cover with length bound
  ;;; find Euler tour
  (let [tree (rotate-tree (metric-tree instance) root)
        dist-mat (metric instance)
        ;;_ (println "starting gs orienteering")
        ;; _ (spit "debug-instance.txt" (prn-str (preprocess-tree tree groups profits
        ;; root)))
        target (+ (min 0.5 (- 1 (apply max (vals profits))))
                  (apply min (vals profits)))
        ;; tansform tree, height and degree reduction
        terminals (apply union (map #(set (second %)) groups))
        _ (assert (>= (count terminals) 1) (prn-str (vals profits)))
        cover (if (> (count terminals) 1)
                (->
                 tree
                 (preprocess-tree groups profits root)
                 (#(greedy-gs (:tree %)
                              (make-group-fn (:profits %) (:groups %))
                              target
                              (:root %))))
                {:nodes terminals})
        tour (tsp-nn dist-mat root (filter #(contains? (set (keys dist-mat)) %) (:nodes cover)))
        _ (if (empty? tour) (throw (Exception. "empty tour")))
        ]
    tour
    )
  )

(def gs-orienteering-mem (memoize gs-orienteering))

;; given an instance of active classification problem
;; outputs
;; 1. sequence of pair of location, "small" observations set
;; and a sequence of partitions

(defn partition-alg [instance root]
  (let [loc-tuples (apply merge
                          (for [loc (locations instance)
                                :let [parts (partition-by-loc instance loc)
                                      [big-set small-set small-keys big-key]
                                      (big-small-sets instance parts)]]
                            ;; DxC and Dx changes every recursive as size of
                            ;; partition changes
                            {loc {:partitions parts
                                  :DxC big-set
                                  :Dx small-set
                                  :small-o small-keys
                                  :big-o big-key}}))
        Bi (apply merge-with union
                  (map
                   (fn [[loc tuple]]
                     (apply merge (map #(hash-map % #{loc}) (:Dx tuple))))
                   loc-tuples))
        path (gs-orienteering instance Bi (priors instance) root)]
    [(map #(vector %
                   (get-in loc-tuples [% :small-o])
                   (get-in loc-tuples [% :big-o])) path)
     (partition-by-path instance path loc-tuples)
     ]))


(defn iso-alg [instance simulator]
  ;(try
    (let [root (location simulator)
          updated (update-instance instance root (observation simulator nil))]
      (if (<= (count (hypotheses updated)) 1)
        [(first (hypotheses updated)) simulator]
        (let [start-time (System/nanoTime)
              [tau parts] (partition-alg updated root)
              end-time (System/nanoTime)]
          ;; traverse tour tau and return directly to r after visiting the
          ;; first location x_k that receive observation o in O_small
          (loop [t tau
                 pp parts
                 sim simulator
                 step-time (/ (- end-time start-time)
                              1000000.0)]
            (if-let [x (first t)]
              ;;
              (let [[loc small-keys _] x
                    next-sim (goto sim loc step-time)
                    obs (observation next-sim nil)]
                (if (some (partial = obs) small-keys)
                  ;; recursive call to iso-alg
                  (iso-alg (split updated ((first pp) obs))
                           next-sim
                           )
                  ;; else continue down the path
                  (recur (rest t) (rest pp) next-sim 0)
                  ))
              ;; else recurse on P_t
              (iso-alg (split updated (peek parts)) sim)
              ))
          )
        ))
    ;(catch Exception e (println "Hypothesis: " (:true-hypothesis simulator) (.getMessage e)))

    )
  ;)
