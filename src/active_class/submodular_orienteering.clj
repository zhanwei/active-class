(ns active-class.submodular-orienteering
  (:require [active-class.problem :refer :all]
            [clojure.set :refer [difference union]]
            [active-class.simulator :refer :all]
            [active-class.gs-greedy-alg :refer [greedy-gs]]
            [active-class.isolation-alg :refer [tsp tsp-nn rotate-tree]]
            [active-class.transform-tree :refer [transform-tree]]
            [active-class.version-sp-fn :refer [make-version-sp-fn]]
            )
)

(defn vs-orienteering-policy [instance start-loc visited]
  (let [dist-mat (metric instance)
        terminals (difference (set (locations instance)) visited)
        sfun (make-version-sp-fn instance)
        cover (->
               (rotate-tree (metric-tree instance) start-loc)
               (transform-tree terminals start-loc)
               (greedy-gs sfun 0.5 start-loc ))
        tour (tsp-nn dist-mat start-loc (filter #(contains? (set (keys dist-mat)) %) (:nodes cover)))
        _ (if (empty? tour) (throw (Exception. "empty tour")))
        ]
    (first tour)
    )
  )


(defn version-space-covering [instance simulator]
  (env-loop instance simulator vs-orienteering-policy))
