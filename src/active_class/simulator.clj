(ns active-class.simulator
  (:require [active-class.problem :refer :all]
            )
)

(defprotocol ActiveClassSimulator
  ;; returns the observation corresponding to underlying hypothesis and
  ;; the distance travelled
  (observation [this randnum])
  (location [this])
  (goto [this location step-time])
  (score [this])
  (history [this])
  (time-history [this]))


(defrecord NoiselessSimulator
    [true-hypothesis
     loc
     distance-total
     instance
     hist
     time-hist
     ]

  ActiveClassSimulator
  (observation [this _]
    (obs instance true-hypothesis loc nil))

  (location [this] loc)

  (goto [this next-loc compute-time] (-> this
                            (assoc :loc next-loc)
                            (assoc :distance-total
                              (+ distance-total
                                 (get-in (metric instance) [loc next-loc] )))
                            (update-in [:hist] conj next-loc)
                            (update-in [:time-hist] conj compute-time)
                            ))
  (score [this] distance-total)
  (history [this] hist)
  (time-history [this] time-hist)
  )

(defn entropy [instance]
  (apply + (map (fn [[_ prob]] (- (* prob (Math/log prob)))) (priors instance)))
  )



(defn make-noiseless-simulator [instance h start-loc]
  (->NoiselessSimulator h start-loc 0.0 instance [start-loc] []))

;; return an updated instance given observation o at location loc,
;; with inconsistent hypotheses removed
(defn update-instance [instance loc o]
  (let [h-set (set (filter #(= o (obs instance % loc nil)) (hypotheses instance)))]
    (split instance h-set)))

;; policy takes as paramter, the new instance, new location and list
;; of locations visited
;;(policy new-instance curr-loc (history ;; simulator))

(defn env-loop [instance_ simulator_ policy]
  (loop [instance instance_
         simulator simulator_
         ]
    (let [curr-loc (location simulator)
          obs (observation simulator nil)
          new-instance (update-instance instance (location simulator) obs)
          ]
      (cond (<= (count (hypotheses new-instance)) 1)
            [(first (hypotheses new-instance)) simulator]
            (= (count (set (history simulator))) (count (locations instance)))
            [nil simulator]
            :else
            (let [start (System/nanoTime)
                  next-loc (policy new-instance curr-loc (history simulator))
                  end (System/nanoTime)
                  ]
              (recur new-instance (goto simulator
                                        next-loc
                                        (/ (- end start)
                                           1000000.0))))
            )
      )
    ))
