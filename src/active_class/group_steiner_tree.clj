(ns active-class.group-steiner-tree
  (:require [clojure.set :refer :all]
            [tree.tree :refer :all]
            [clojure.string :as str]
            [clojure.zip :refer [zipper node branch? up down end? root left
                                 right insert-child insert-right children]
             :as zip]
            [loom.graph :refer :all]
            [loom.alg :refer [post-traverse]]
            [active-class.transform-tree :refer [transform-tree]]
            )
  )

(defn gen-rand-groups [nodes num-groups group-size]
  (loop [n num-groups
         groups {}]
    (if (zero? n)
      groups
      (recur (dec n)
             (assoc groups n
                    (loop [i (int (inc (rand group-size))) g #{}]
                      (if (zero? i)
                        g
                        (recur (dec i)
                               (conj g ((vec nodes) (int (rand (count nodes))
                                                         ))))))))))
  )

(defrecord GroupSteinerTree
    [;; loom directed weighted graph
     tree
     ;;map of integer to sets as mapping from group-id to set of vertices in group
     groups
     ;;map from group-id to profit associated with the group
     profits
     ;;root node of the tree
     root])


(defn gen-rand-gs-tree [depth limit branch group-size num-groups]
  (let [[tree num-nodes] (gen-rand-tree depth branch 0 limit)
        weights (into {} (for [n (range limit)]
                           [n (inc (int (rand 100)))]))
        groups (gen-rand-groups (range num-nodes) num-groups group-size)
        profits (into {} (map vector (keys groups) (repeatedly #(rand 10))))
        ]
    (->GroupSteinerTree (convert-to-graph (make-zip-tree tree) weights) groups profits 0))
  )

(defn is-terminal? [groups node]
  (some #(contains? (second %) node) groups))


(defn eliminate-non-terminal-leaf [tree groups root]
  (loop [post (post-traverse tree root)
         curr-tree tree]
    (if-let [curr-node (first post)]
      (if (and (empty? (successors curr-tree curr-node)) (not (is-terminal? groups curr-node)))
        (recur (rest post) (remove-nodes curr-tree curr-node))
        (recur (rest post) curr-tree))
      curr-tree
      ))
  )


(defn eliminate-degree-two-interior [tree groups]
  (loop [post-order (post-traverse tree)
         curr-tree tree]
    (if-let [curr-node (first post-order)]
      (if (and
           (= 1 (out-degree curr-tree curr-node))
           (= 1 (in-degree curr-tree curr-node))
           (not (is-terminal? groups curr-node)))
        (let [pred (first (predecessors curr-tree curr-node))
              succ (first (successors curr-tree curr-node))]
          (recur (rest post-order)
                 (->
                  curr-tree
                  (add-edges [pred
                              succ
                              (+ (weight curr-tree pred curr-node)
                                 (weight curr-tree curr-node succ))])
                  (remove-nodes curr-node))))
        (recur (rest post-order) curr-tree)
        )
      curr-tree
      ))
  )


(defn add-dummy-group
  "Takes as input seq of nodes, exisitng cover, and number of
  groups. Create a dummy group with all the vertex in it. Make sure
  the profits of dummy group is the minimum"
  [nodes groups profits root]
  [(conj groups [:dummy (set (remove (partial = root) nodes))])
   ;;(conj groups [:dummy nodes])
   (conj profits [:dummy (apply min  (vals profits)) ])]
  )

(defn preprocess-tree [tree groups profits root]
  (let [post-tree
        (-> tree
            ;;Eliminate every non terminal leaf
            (eliminate-non-terminal-leaf groups root)

            ;;Eliminate every non terminal interior vertex v of degree two
            ;; p = x - v -y becomes x - y
            (eliminate-degree-two-interior groups)
            ;;(transform-tree groups root)
            )
        [post-groups post-profits]
        (add-dummy-group (nodes post-tree) groups profits root)
        terminals (apply union (map #(set (second %)) post-groups))
        ]
    (->GroupSteinerTree (transform-tree post-tree terminals root) post-groups post-profits root)
    ))

(defn view-group-steiner-tree
  [tree weights groups]
  (view-tree tree weighted-digraph
             #(if (is-terminal? groups (data %))
                (str/join ["a:" (data %)])
                (data %))
             #(weights (data %)))
  )
