(ns active-class.submodular-fn)

(defprotocol SubmodularFn
  (update-fn [this new-items])
  (profits [this items])
  (min-profit [this])
  )
