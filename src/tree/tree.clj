(ns tree.tree
  (:require [clojure.zip :refer [zipper node branch? up down end? root left
                                 right insert-right children insert-child]
             :as zip]
            [loom.graph :refer :all]
            [loom.io :refer :all]))

(defn data [tree]
  (peek (node tree)))

;; replace data at loc in tree with the value of (f data)
(defn edit-data
  ([tree f]
     (zip/replace tree
               (-> (node tree)
                   pop
                   (conj (f (data tree))))))
  ([tree f key]
     (edit-data tree #(update-in % [key] f))))


(defn make-zip-tree
  "zip-tree from a post-ordered list"
  [postorder-list]
  (zipper #(> (count %) 1)
          pop
          (fn [n c] (conj (vec c) (peek n)))
          postorder-list
          )
  )

(defn gen-rand-tree
  [depth branch-factor start-index limit]
  (if (zero? depth)
    [[start-index] (inc start-index)]
    (loop [curr (make-zip-tree [[:dummy] start-index])
           b (inc (int (rand branch-factor)))
           last-index (inc start-index)
           ]
      (if (or (zero? b) (> last-index limit))
        ;;remember to remove the dummy child
        [(root (zip/remove (zip/rightmost (down curr)))) last-index]
        ;;[(root curr) last-index]
        (let [[child latest]
              (gen-rand-tree (int (rand depth)) branch-factor last-index limit)]
          (recur (insert-child curr child)
                 (dec b)
                 latest))
        )
      )
    ))


(defn convert-to-graph
  ([tree constructor-fn label-fn weight-fn]
     (loop [t tree
            g (constructor-fn)]
       (if (end? t)
         g
         (recur (zip/next t)
                (if (up t)
                  (add-edges g [(label-fn (up t))
                                (label-fn t)
                                (weight-fn t)]
                             )
                  g)
                )))
     )
  ([tree]
     (convert-to-graph tree digraph data identity))
  ([tree weights]
     (convert-to-graph tree weighted-digraph data #(weights (data %)))
     )

  )

(defn view-tree
  ([tree constructor-fn label-fn weight-fn]
     (view
      (convert-to-graph tree constructor-fn label-fn weight-fn))
     )
  ([tree]
     (view-tree tree digraph data identity))
  ([tree weights]
     (view-tree tree weighted-digraph data #(weights (data %)))
     )

  )
