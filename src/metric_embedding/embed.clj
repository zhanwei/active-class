(ns metric-embedding.embed
  (:require [loom.graph :refer :all]
            [loom.gen :refer :all]
            [clojure.set]
            [clojure.math.numeric-tower :refer [ceil floor]]
            [clojure.zip :as zip]
            [incanter.core :refer [log2 pow log]]
            [loom.alg :refer [bellman-ford]]))


;;(def rwg (gen-rand (weighted-graph) 10 20 :max-weight 100))

;; calculate the the conditional probability of Di separates (u,v)
;; give beta and prefix of permutation of vertices
;; prefix may be empty
(defn- prob-separates [i dist-mat u v beta prefix]
  (let [d-u-v (get-in dist-mat [u v])
        j-star (- (log2 d-u-v) 2)
        Bi (* beta (pow 2 (dec i)))
        ;; calculate the ratio of centres that cut to centres that settle both edges
        prob-cut
        (fn [u1 v1 vertices]
          (let [s (filter #(<= (get-in dist-mat [v1 %]) Bi)
                          vertices)
                m (filter #(and (> (get-in dist-mat [v1 %]) Bi)
                                (<= (get-in dist-mat [u1 %]) Bi))
                          vertices)
                ]
            (/ (count m) (+ (count m) (count s)))
            )
          )
        ]
    (if (< i j-star)
      ;; u and v cannot be in the same cluster in Di+1
      0
      ;; check if u or v already assign to any of the vertices in the
      ;; permutation
      (let [cu (some #(if (<= (get-in dist-mat [u %]) Bi) %) prefix)
            cv (some #(if (<= (get-in dist-mat [v %]) Bi) %) prefix)]
        (if (or cu cv)
          (if (= cu cv)
            0 ;; u and v to same cluster
            1) ;; else they are already separated
          ;; else calculate the conditional probability
          (let [vertices-remain (clojure.set/difference (set (keys dist-mat))
                                                        (set prefix))
                prob-v-cut (prob-cut v u vertices-remain)
                prob-u-cut (prob-cut u v vertices-remain)
                ]
            (+ prob-v-cut
               prob-u-cut))
          )
        )
      ))
  )

(defn- get-edges [dist-mat]
  (let [num-nodes (count (keys dist-mat))
        ns (vec (keys dist-mat))]
    (for [ui (range num-nodes) vi (range num-nodes)
          :when (< ui vi)
         :let [u (ns ui) v (ns vi)
               dist (get-in dist-mat [u v])]
         :when (not= dist 0)]
     [u v])))


;; Compute the graph distance matrix for weighted un-directed graph
(defn graph-distance-matrix [graph]
  (apply merge (map (fn [start] (->> (bellman-ford graph start)
                                     first
                                     (hash-map start)))
                    (nodes graph)))
  )

;; calculate the conditional expected cost of an edge give beta and
;; prefix of permutation of vertices
(defn- expected-edge-cost [dist-mat u v beta prefix delta]
  (apply + (for [i (range  delta)] (* (pow 2 (+ 2 i))
                                     (prob-separates i dist-mat u v beta prefix)
                                     )))
  )

(defn- expected-tree-cost [dist-mat beta prefix delta]
  (apply + (for [e (get-edges dist-mat) :let [[u v] e]]
             (expected-edge-cost dist-mat u v beta prefix delta))))

(defn graph-diameter [distance-matrix]
  (apply max (map (fn [start] (->> (distance-matrix start)
                         vals
                         (apply max))) (keys distance-matrix))))

;; choose the beta that minimizes (weighted?) edge stretch
(defn- choose-beta [dist-mat delta average]
  (let [beta-candidates
        (set (map #(/ % (pow 2 (floor (log2 %))))
                  (filter (partial <= 2)
                          (set (for [e (get-edges dist-mat)]
                                 (get-in dist-mat e)
                                 )))))
        _ (println beta-candidates)
        beta-cost (into {}
                        (map #(vector % (expected-tree-cost dist-mat % [] delta))
                             beta-candidates))
        min-beta (apply min-key second beta-cost)
        ;; _ (println (map #(expected-tree-cost dist-mat % [] delta) beta-candidates))
        ]
    min-beta
    )
  )

(defn- choose-prefix [dist-mat beta delta remain prefix average]
  (if (seq remain)
    (let [next (some
                #(if (< (expected-tree-cost dist-mat beta
                                            (conj prefix %) delta)
                        average) % (println %)) remain )
          ]
      (cons next
            (lazy-seq
             (choose-prefix dist-mat beta delta (disj remain next)
                            (conj prefix next) average))))
    nil))

(defn choose-parameters [dist-mat delta]
  (let [total-cost
        (reduce + (for [e (get-edges dist-mat)
                        :let [dist (get-in dist-mat e)]
                        ]
                    dist
                    ))
        average (* (log2 (count dist-mat)) total-cost)
        _ (println "average is " average)
        _ (println "total cost is " total-cost)
        [beta average1] (choose-beta dist-mat delta average)
        _ (println beta)]
    [beta (choose-prefix dist-mat beta delta (set (keys dist-mat)) [] average1)])
  )

;; generate new distance matrix for a sub-graph
(defn- subgraph-diameter [dist-mat nodes]
  (let [n (vec nodes)]
    (apply max (for [i (range (count nodes))
                     j (range (inc i) (count nodes))
                     ]
                 (get-in dist-mat [(n i) (n j)]))))
  )

;; Embed arbitrary metric into a tree metric
;; outputs a loom.graph representation of the tree
(defn graph-partition [dist-mat beta delta perm]
  (letfn
      [(distance-fn [u v] (get-in dist-mat [u v]))
       (partition-cluster [tree cluster tree-distance]
         (loop [centres perm
                unassigned cluster
                curr-tree tree]
           (if (and (first centres) (seq unassigned))
             (let [centre (first centres)
                   Bi (* beta (/ tree-distance 2))
                   [new-cluster remain]
                   (map (group-by #(< (distance-fn %1 centre) Bi)
                                  unassigned) [true false])
                   new-cluster-size (count new-cluster)]
               (let [next-tree
                     (cond
                      (zero? new-cluster-size)
                      ;; nothing to add
                      curr-tree

                      (= 1 new-cluster-size)
                          ;;; get the item for the list, so that it
                          ;;; appears the same as the node in original graph
                      (add-edges curr-tree [cluster (first new-cluster) (max 1 (dec tree-distance))])

                      (> new-cluster-size 1)
                       (if (= new-cluster cluster)
                        ;;take care of weight
                         (partition-cluster
                          (add-edges curr-tree [cluster new-cluster
                                                0])
                          new-cluster
                          (/ tree-distance 2))
                        (let [new-diameter (subgraph-diameter dist-mat new-cluster)
                              new-tree-distance (pow 2 (dec (ceil (log2 new-diameter))))
                              ]
                          (partition-cluster
                           (add-edges curr-tree [cluster new-cluster tree-distance])
                           new-cluster
                           new-tree-distance
                           ))
                      ))]
                 (recur (rest centres)
                        remain
                        next-tree))
               )
             ;;else
             curr-tree)
           )
         )

       (remove-loops [tree]
         (loop [es (edges tree)
                curr-tree tree]
           (if-let [e (first es)]
             (let [next-tree
                   (if (apply = e)
                     (let [w1 (apply weight curr-tree e)
                           source (first e)
                           removed (remove-edges curr-tree e)]
                       (reduce (fn [t succ]
                                 (let [w2 (weight t source succ)]
                                   (add-edges t [source succ (+ w1 w2)])))
                               removed
                               (successors removed source))
                       )
                     curr-tree)]
               (recur (rest es) next-tree))
             curr-tree)
           ))

       ;; replace label of nodes with integer
       (replace-node [tree root]
         (let [new-id-map
               (into {}
                     (map vector
                          (filter coll? (nodes tree))
                          (iterate inc (count root))))
               ]
           (apply add-edges (weighted-digraph)
                  (for [e (edges tree)
                        :let [w (apply weight tree e)]]
                    (conj (mapv #(if (coll? %) (new-id-map %) %) e)
                          w)))
           )
         )
       ]
    (-> (partition-cluster (weighted-digraph) (keys dist-mat) (pow 2 (dec delta)))
        remove-loops
        (replace-node (keys dist-mat))
        ))
  )



;; randomized version of graph partition
(defn graph-partition-rand [dist-mat]
  (let [rand-perm (shuffle (keys dist-mat)) ;;random permutation of vertices
        beta (inc (rand))
        delta  (ceil (log2 (graph-diameter dist-mat)))
        ]
    (graph-partition dist-mat beta delta rand-perm)
    ))

;; derandomized version of graph partition
(defn graph-partition-derand [dist-mat]
  (let [delta (ceil (log2 (graph-diameter dist-mat)))
        [beta perm] (choose-parameters dist-mat delta)
        result (graph-partition dist-mat beta delta perm)
        ]
    result
    )
  )
