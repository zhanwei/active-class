(ns domains.grid-search
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer [intersection union]]
            [incanter.core :refer :all]
            [metric-embedding.embed :refer :all]
            ;; [incanter.charts :refer [scatter-plot]]
            ))


(defn make-grid-graph [l w h dist]
  (let [ns (for [x (range l)
                 y (range w)
                 z (range h)]
             [x y z])
        node-id (into {} (map vector ns (range)))
        g (weighted-graph)
        edges (for [src ns
                    delta [[0 0 1] [0 1 0] [1 0 0]
                           [0 0 -1] [0 -1 0] [-1 0 0]]
                    :let [target (vec (map + delta src))]
                    :when (and (every? true? (map < target [l w h]))
                               (every? true? (map >= target (repeat 0))))]
                [(node-id src) (node-id target) dist]
                )
        ]
    [(apply add-edges g edges) (vec ns)]
    ))

;; returns the distance between two 3D location tuple
(defn distance-loc [start end]
  (sqrt (apply + (map #(* % %) (map - start end))))
  )

;; returns the distance matrix of a grid space with length l, width w, height h
(defn make-grid-metric [l w dist-height dist-stretch]
  (let [ns (vec (for [z (range 2)
                      x (if (zero? z) (range l)
                            (range l))
                      y (if (zero? z) (range w)
                            (range w))
                      ]
                  [x y (* z dist-height)]))]
    [(into {} (for [start (range (count ns))
                    :let [start-loc (ns start)]]
                [start
                 (reduce (fn [cost end]
                           (conj cost [end
                                       (if (and (not= 0 (start-loc 2))
                                                (not= 0 ((ns end) 2)))
                                         (distance-loc start-loc (ns end))
                                         ;; low level flying is slower...
                                         (* dist-stretch (distance-loc start-loc (ns end))))]))
                         {} (range (count ns)))]
                ))
     ns])
  )

(defn rand-range [start end]
  (+ start (rand-int (- end start)))
  )

(defn generate-trail [start end]
  (if (some #(> % 3) (map abs (map - start end)))
    (let [bounding-box (map sort (map vector start end))
          mid (mapv #(apply rand-range %) bounding-box)]
      (into (generate-trail start mid) (generate-trail mid end)))
    (loop [path [start]]
      (let [delta (map - (peek path) end)
            _ (println delta)]
        (if (every? zero? delta)
          path
          (recur (conj path (mapv + (peek path) (map #(cond
                                                       (<= % -1) 1
                                                       (>= % 1) -1
                                                       :else 0)
                                                     delta))))))
      )
    )
  )
(comment
  (defn view-trail [trail]
    (with-data (to-dataset trail)
      (doto (scatter-plot ($ :col-0) ($ :col-1))
        view))))

(defrecord RescueProblem
    [survivor-locs
     distance-matrix
     id-loc ;;map from node id to location tuple
     noisy-locs ;;locations that generates false positive using long range sensor
     trails;; path in the graph where presence of positive observation
     ;; indicate that survivor is on the trail
     loc-trails ;; a map of location to the set of trails it belongs to
     loc-positive ;; a map of location to true or false of whether to
     ;; reveal evidence of survivor
     tree
     ]
  ActiveClass
  (hypotheses [this] survivor-locs)

  (locations [this] (set (range (count id-loc))))

  (priors [this] (let [no-locs-left (count survivor-locs)
                       prob (/ 1 no-locs-left )]
                   (into {} (map #(vector % prob) survivor-locs))))

  (obs [this h loc _]
    (let [[x y z] (id-loc loc)]
      (if (not= z 0)
        ;; long range observation
        (some true? (map (fn [oloc] (and (= oloc h) (not (contains? noisy-locs oloc))))
                         (map #(map + %1 %2) (repeat [x y])
                              (for [dx [1 0 -1]
                                    dy [1 0 -1]]
                                [dx dy]))))
        ;; true or false and special obs
        (if (= [x y] h)
          true
          (if (and (loc-positive [x y])
                   (seq loc-trails)
                   (seq (intersection (loc-trails [x y]) (loc-trails h))))
            :evidence
            false))
        )))

  (metric [this] distance-matrix)

  (metric-tree [this] @tree)

  (split [this new-h-set]
    (assoc this :survivor-locs (set new-h-set)))
  )

;; output map of location to set of trails it belongs to
(defn make-loc-trails [trails]
  (apply merge-with union
         (for [i (range (count trails))]
           (reduce (fn [m loc] (conj m [loc #{i}])) {} (trails i))))
  )

(defn make-rescue-problem
  ([size num-trails dist-h dist-stretch]
     (let [num-hypotheses (* size size)
           locations (for [x (range size)
                           y (range size)]
                       [x y])
           ;; generate num-trails trails
           trails (let [perm (shuffle locations) ]
                    (vec (for [i (range num-trails)]
                           (let [start (nth perm i)
                                 end (nth perm (inc i))
                                 _ (println i)]
                             (generate-trail start end)))))
           loc-trails (make-loc-trails trails)
           [distance-matrix id-loc] (make-grid-metric size size dist-h dist-stretch)
           ]
       (->RescueProblem
        locations
        distance-matrix
        id-loc
        ;; randomly pick 1/4 H locations to give  noisy observation
        (set (take (/ num-hypotheses 4) (shuffle locations)))
        trails
        loc-trails
        (into {} (for [[loc _] loc-trails]
                   [loc (< (rand) 0.3)] ))
        (delay (graph-partition-derand distance-matrix))
        )))

  ([size trails loc-positive noisy-locs dist-h dist-stretch]
     (let [[distance-matrix id-loc] (make-grid-metric size size dist-h dist-stretch)]
       (->RescueProblem
       ;; location
       (for [x (range size)
             y (range size)]
         [x y])
       distance-matrix
       id-loc
       noisy-locs
       trails
       (make-loc-trails trails)
       loc-positive
       (delay (graph-partition-derand distance-matrix))
       ))
     )

  )
