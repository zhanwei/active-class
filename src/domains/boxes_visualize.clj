(ns domains.boxes-visualize
  (:require [quil.core :refer :all]
            [domains.boxes :refer [gen-boxes-locs]])
  )

(def box-sizes [[5 4] [3 3]] )

(def boxes-locs (vec (gen-boxes-locs box-sizes [10 12])))

(def curr (atom 0))

(defn draw-configs [locs sizes]
  (doseq [param (map into locs sizes)]
    (apply rect (map (partial * 10) param)))
  )

(defn setup []
  (smooth)
  (frame-rate 1)
  (background 200))

(defn draw []
  (stroke 20)
  (stroke-weight 2)
  (fill 200)
  (rect 0 0 323 200)
  (fill 10 255 10)
  (if (<= @curr (count boxes-locs))
    (do (draw-configs (boxes-locs @curr) box-sizes)
        (swap! curr inc)))
  )

(defsketch example
  :title "My boxes"
  :setup setup
  :draw draw
  :size [323 200])
