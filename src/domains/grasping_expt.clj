(ns domains.grasping-expt
  (:require [active-class.problem :refer :all]
            [active-class.simulator :refer :all]
            [domains.grasping :refer :all]
            [domains.grasping-model :refer :all]
            ;;[domains.draw-grasping :refer :all]

            [active-class.isolation-alg :refer [iso-alg]]
            [experiment.runner :refer :all]
            [experiment.baseline :refer :all]
            [clojure.math.numeric-tower :refer [sqrt]]
            )
  )

(def PAN_COST 5)
(def TILT_COST 1)

(def cup-centered (translate-model cup-with-handle 15 15))
(def cup-no-handle-centered (translate-model cup-no-handle 15 15))

(def cup-models (vec (concat [cup-centered]
                              ;; rotate the cup that has handle
                             (for [r (range 5 360 5)]
                               (rotate-model cup-centered r [35 35])))))

;;; generate cups of workspace
;;; consisting of two cups, one of which has a handle
(def two-cups-model
  (vec (concat (for [c cup-models]
                 (concat-model [cup-no-handle-centered
                                (translate-model c 55 20)]))
               (for [c cup-models]
                 (concat-model [(translate-model cup-no-handle-centered 55 20)
                                c]))
               )))

(defn uniform-priors [num-hypotheses]
  (let [prob (/ 1 num-hypotheses)]
    (reduce #(conj %1 [%2 prob]) {} (range num-hypotheses)))
  )

(def nonuniform-prior (let [high-probs (concat (range 70 72) (range 8))
                            ]
                        (-> (reduce
                             #(assoc %1 %2 (* 0.7 (/ 1 (count two-cups-model))))
                             (uniform-priors (count two-cups-model))
                             (range (count two-cups-model)))
                            (#(reduce
                               (fn [p h](update-in p [h] (partial + (/ 0.3 (count high-probs)))))
                               %
                               high-probs)))))

(def problem (make-grasping-problem two-cups-model
                                    ;;(uniform-priors (count two-cups-model))
                                    nonuniform-prior
                                    [125 90]
                                    PAN_COST
                                    TILT_COST
                                    ))
