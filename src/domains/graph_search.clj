(ns domains.graph-search
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer [intersection union]]
            [incanter.core :refer :all]
            [metric-embedding.embed :refer [graph-distance-matrix]]
            ;; [incanter.charts :refer [scatter-plot]]
            ))


(def seq-centre 0)
(def bin-centre 1)
(defn seq-node-id [q i] (+ 2 i))
(defn bin-node-id [q i] (+ 2 (pow 2 q) i))
;; problem consist of hypotheses 0 < h < 2^q
;; a set of q binary detection locations i with returns the i-th bit of h
;; a set of 2^q sequential detection locations j, returns whether h=j

(defrecord GraphProblem
    [locs
     tree
     h-set
     binary-locs ;; location where observations are sequential
     sequential-locs ;; location where observations are sequential
     distance-matrix
     ]
  ActiveClass
  (hypotheses [this] h-set)

  (locations [this] locs)

  (priors [this] (let [no-locs-left (count h-set)
                       prob (/ 1 no-locs-left )]
                   (into {} (map #(vector % prob) h-set))))

  (obs [this h loc _]
    (if-let [obit (binary-locs loc)]
      (bit-test h obit)
      ;; sequential observation
      (if (= h (sequential-locs loc))
        1
        0)
      )
    )

  (metric [this] distance-matrix)

  (metric-tree [this] tree)

  (split [this new-h-set]
    (assoc this :h-set (set new-h-set)))
  )



(defn make-star-graph [q dist graph-fn]
  (-> (graph-fn)
      ;; add sequential locations
      ((fn [g]
         (apply add-edges g
                (for [i (range (pow 2 q))]
                  [seq-centre (seq-node-id q i) 1]))
         ))
      ;; add binary locations
      ((fn [g]
         (apply add-edges g
                (for [i (range q)]
                  [bin-centre (bin-node-id q i) 1]))
         ))
      ;; add the edge between seq-centre and bin-centre
      (add-edges [seq-centre bin-centre dist])
      ))

;; make a two star graph takes q and dist as parameter. dist specifies
;; the distance between the two centres of the star

(defn make-graph-problem [q dist]
  (let [h-set (range (pow 2 q))
        graph (make-star-graph q dist weighted-graph)
        binary-locs (into {} (map #(vector (bin-node-id q %) %) (range q)))
        ]
    (->GraphProblem (nodes graph)
                    (make-star-graph q dist weighted-digraph)
                    h-set binary-locs
                    (into {} (map #(vector (seq-node-id q %) %) (range (pow 2 q))))
                    (graph-distance-matrix graph) )
    )
  )
