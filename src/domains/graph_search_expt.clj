(ns domains.graph-search-expt
  (:require [active-class.problem :refer :all]
            [active-class.simulator :refer :all]
            [domains.graph-search :refer :all]
            [active-class.isolation-alg :refer [iso-alg]]
            [experiment.runner :refer :all]
            [experiment.baseline :refer :all]
            )
  )

(comment
  (def problem (make-graph-problem 8 73))

(def test-small-problem (make-graph-problem 5 10))
)
(defn run-iso [problem]
  (run-allp iso-alg problem seq-centre))

(defn run-greedy-path [problem k]
  (run-allp (greedy-path-fn k) problem seq-centre))

(defn run-iso-once [problem h]
  (run-once iso-alg problem seq-centre h))

(defn run-greedy-path-once [problem k h]
  (run-once (greedy-path-fn k) problem seq-centre h))

(defn main [i]
  (doseq [i [[6 53] [7 53] [8 53] [6 10] [5 10] [8 73]]]
    (let [pb (apply make-graph-problem)
          hs (hypotheses pb)]
      (do (run-all iso-alg pb seq-centre)
          (run-all greedy-loc pb seq-centre)
          (run-all greedy-distance pb seq-centre)))))
