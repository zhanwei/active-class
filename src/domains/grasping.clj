(ns domains.grasping
  (:require [active-class.problem :refer :all]
            [clojure.set :refer [intersection union]]
            [metric-embedding.embed :refer :all]
            [domains.grasping-model :refer :all]
            [loom.graph :refer [weighted-digraph weighted-graph add-edges]]
            ))


(defn laser-distance [model [laser-pos laser-angle]]
  (->>
   (:vectors model)
   (reduce #(concat %1 (laser-intersection %2 laser-pos laser-angle)) [])
   (remove nil?)
   (map #(map - laser-pos %))
   (map #(Math/hypot (float (first %)) (float (second %))) )
   (#(if (empty? %)
       (Integer/MAX_VALUE)
       (Math/round (float (apply min %)))))
   ))


(defn gen-valid-poses [pos-region models]
  (let [[max-x max-y] pos-region]
    (reduce into '()
            (for [m (range (count models))]
              (for [x (range 1 (- max-x (first (:dim (models m)))))
                    y (range 1 (- max-y (second (:dim (models m)))))
                    :when (let [[dx dy] (:dim (translate-model (models m) x y))]
                            (not (or (> dx max-x) (> dy max-y))))]
                [m x y])
              ))))


(defrecord GraspingProblem
    [workspace-dim
     locs
     arm-positions
     models
     state-priors
     tree
     distance-matrix
     ]
  ActiveClass
  (hypotheses [this] (set (keys state-priors)))

  (locations [this] (range (count locs)))

  (priors [this] state-priors)

  (obs [this h loc-id _]
    (let [loc (locs loc-id)
          model (models h)]
      (laser-distance model [(arm-positions (first loc) ) (second loc)])))

  (metric [this] distance-matrix)

  (metric-tree [this] tree
    )
  (split [this new-h-set]
    (assoc this :state-priors
           (let [total-prob (apply + (vals (select-keys state-priors new-h-set)))]
             (reduce #(conj %1 [%2 (/ (state-priors %2) total-prob)])
                     {}
                     new-h-set)
             )))
  )

(defn make-grasping-problem [models priors workspace-dim PAN_COST TILT_COST]
  (let [width (first workspace-dim)
        height (second workspace-dim)
        arm-positions [
                       ;; 0 top left
                       [0 0]
                       ;; 1 left
                       [0 (int (/ height 2))]
                       ;; 2 bottom left
                       [0 height]
                       ;; 3 bottom
                       [(int (/ width 2)) height]
                       ;; 4 bottom right
                       [width height]
                       ;; 5 right
                       [width (int (/ height 2))]
                       ;; 6 top right]
                       [width 0]
                       ]
        locs (vec (for [i (range 7)
                        t (range 5 360 5)
                        :when (and (not= 0 (rem t 90))
                                   (let [[x0 y0] (arm-positions i)
                                         dir (if (>= t 180) -1 1)
                                         y1 (+ y0 (* dir (Math/tan (Math/toRadians t))))
                                         x1 (+ x0 dir)]
                                     (and (>= x1 0) (<= x1 width)
                                          (>= y1 0) (<= y1 height)))
                                   )]
                    [i t]))
        hs (set (range (count models)))
        graph-edges (concat (for [c (range 1 7)
                                  :let [start (+ (dec c) (count locs))
                                        end  (+ c (count locs))]]
                              (if (or (= c 3) (= c 4))
                                [start end (* PAN_COST (int (/ width 2)))]
                                [start end (* PAN_COST (int (/ height 2)))]))
                            (for [l (range (count locs))]
                              [(+ (first (locs l)) (count locs))
                               l
                               TILT_COST])
                            )
        ]
    (->GraspingProblem
     workspace-dim
     locs
     arm-positions
     models
     priors
     (apply add-edges (weighted-digraph) graph-edges)
     (let [distances (for [c (range 1 7)]
                       (if (or (= c 3) (= c 4))
                         (/ width 2)
                         (/ height 2)))
           cum-distance (vec (map int (reduce #(conj %1 (+ (peek %1) %2)) [0]
                                              distances)))
           ]
       (into {}
             (for [start (range (count locs))]
               [start
                (into
                 {}
                 (for [end (range (count locs))]
                   [end
                    (let [start-centre (first (locs start))
                          end-centre (first (locs end))]
                      (+ (* PAN_COST (Math/abs (- (cum-distance start-centre)
                                                  (cum-distance end-centre))))
                         (* 2 TILT_COST))
                      )]))])))
     ))
  )
