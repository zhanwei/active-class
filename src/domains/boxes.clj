(ns domains.boxes
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer [intersection union]]
            [incanter.core :refer :all]
            [metric-embedding.embed :refer :all]
            ;; [incanter.charts :refer [scatter-plot]]
            ))

;; assume there are only two boxes
(defn- not-in-range [start end i]
  (not (and (<= start i) (>= end i))))

(defn gen-boxes-locs [sizes ws-size]
  (let [[max-x max-y] ws-size
        [[h1 w1] [h2 w2]] sizes]
    (for [x1 (range (- max-x w1))
          y1 (range (- max-y h1))
          ;; include a bounding box
          x2 (range (- max-x w2))
          y2 (filter #(and (not-in-range (dec y1) (+ y1 h1 1) %)
                           (not-in-range (dec y1) (+ y1 h1 1) (+ % h2)))
                     (range (- max-y h2)))
          ]
      [[x1 y1] [x2 y2]]))
  )

(defrecord BoxesProblem
    [workspace-size
     box-sizes
     boxes-locs ;;locations are specified by top-left corner
     ]
  ActiveClass
  (hypotheses [this] boxes-locs)

  (locations [this] (set (range workspace-size)))

  (priors [this] (let [no-locs-left (count boxes-locs)
                       prob (/ 1 no-locs-left )]
                   (into {} (map #(vector % prob) boxes-locs))))

  (obs [this h loc _]
    nil)

  (metric [this] nil)

  (metric-tree [this] nil)

  (split [this new-h-set]
    (assoc this :boxes-locs (set new-h-set)))
  )
