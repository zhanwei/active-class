(ns domains.draw-grasping
  (:require [domains.grasping-model :refer :all]
            [domains.grasping :refer :all]
            [active-class.problem :refer :all]
            [quil.core :as qc]
            [active-class.simulator :refer [update-instance
                                            make-noiseless-simulator
                                            history
                                            score]]))

(def SCALE 5)

(defn get-points
  [instance]
  (reduce
   (fn [ps model]
     (clojure.set/union ps (set (:points model))))
   #{}
   (map (:models instance) (hypotheses instance)))
  )

(defn scale-translate [i] (+ (* i SCALE) SCALE))

(defn draw-workspace [loc instance]
  (let [points-set (get-points instance)
        ]
    (qc/background-float 125)
    (qc/stroke-weight 3)
    (qc/stroke-float 10)
    (doseq [pt points-set]
      (apply qc/point (map scale-translate pt)))
    ;; change fill color
    (qc/stroke-weight 1)
    (qc/fill-float 255 0 0)
    (let [[ci angle] ((:locs instance) loc)
          pos ((:arm-positions instance) ci)
          pt (map scale-translate pos)
          end-loc   (let [width (first (:workspace-dim instance))
                          dir (if (>= angle 180) (- width) width)
                          [x0 y0] pos
                          y1 (+ y0 (* dir (Math/tan (Math/toRadians angle))))
                          x1 (+ x0 dir)
                          _ (println x1 y1 x0 y0 angle)]
                      [x1 y1])
          pt-end (map scale-translate end-loc)]
      (qc/rect (first pt) (second pt) SCALE SCALE)
      (qc/line (first pt) (second pt) (first pt-end) (second pt-end))
      )
    ))

(defn visualize-simulation [instance history true-hypothesis]
  (let [time-slices (atom history)
        curr-instance (atom instance)]
    (qc/defsketch visulization
      :title "Visualization"
      :size (vec (map #(+ (* 2 SCALE) (* SCALE %)) (:workspace-dim instance)))
      :setup (fn []
               (qc/smooth)
               (qc/background-float 125)
               (qc/frame-rate 0.5)
               )
      :draw (fn drawfn []
              (let [loc (first @time-slices)]
                (draw-workspace loc @curr-instance)
                (swap! curr-instance
                       (fn [_]
                         (update-instance @curr-instance
                                          loc
                                          (obs @curr-instance true-hypothesis loc nil)
                                          )))
                (swap! time-slices (fn [_] (rest @time-slices))))
              ))
    )
  )

(defn visualize-once [alg problem start-loc hypothesis]
  (let [sim (make-noiseless-simulator problem hypothesis start-loc)
        result (time (alg problem sim))]
    (do (println (history (second result)))
        (println "cost is " (score (second result)))
        (visualize-simulation problem (history (second result)) hypothesis)
        (if (not= (first result) hypothesis)
          (do
            (println "wrong hypothesis got"
                     (first result))))
        (score (second result))))
  )
