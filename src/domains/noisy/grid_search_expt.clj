(ns domains.noisy.grid-search-expt
  (:require [active-class.problem :refer :all]
            [active-class.noisy.simulator :refer :all]
            [domains.noisy.grid-search :refer :all]
            [active-class.noisy.isolation-noisy :refer [iso-deterministic-alg iso-ml-alg]]
            [active-class.noisy.edge-cutting-isolation :refer [get-iso-ec-alg]]
            [experiment.noisy.runner :refer :all]
            [experiment.noisy.baseline :refer :all]
            [clojure.math.numeric-tower :refer [sqrt]]
            [active-class.noisy.edge-cutting-isolation :refer :all]
            )
  )

;; noisy locs derived from
(def noisy-locs #{[3 3] [4 3] [5 3] [3 4] [4 4] [5 4] [3 5][4 5] [5 5] [6 3] [7 3] [6 4] [7 4] [6 5] [7 5]})

(def problem (assoc (make-rescue-problem-noisy 8 noisy-locs 10 4)
               :tree (delay (read-string (slurp "new-grid-search-graph.dat")))))



(def small-problem (make-rescue-problem-noisy 4 #{} 10 4)
  )

(defn output-tikz-trace [trace]
  (let [coords (map #(if (not= 0 (% 2))
                       (str "top-" (% 0) "-" (% 1))
                       (str "bottom-" (% 0) "-" (% 1)))
                    trace)]
    (do
      (print "\\draw ")
      (doseq [c coords]
        (print (str "(" c  ")") " -- " )
        ))))


(defn output-tikz-noisy-loc [noisy-locs]
  (do
    (doseq [l noisy-locs]
      (print (str (first l) "/" (second l) ) ", " )
      )))

(comment (run-once iso-ml-alg small-problem 10 [0 1]))
