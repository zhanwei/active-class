(ns domains.noisy.grid-search
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer [intersection union]]
            [incanter.core :refer :all]
            [domains.grid-search :refer :all]
            [metric-embedding.embed :refer :all]
            ;; [incanter.charts :refer [scatter-plot]]
            ))

(def ^:dynamic noisy-level 0.05)

(defrecord RescueProblemNoisy
    [survivor-priors
     distance-matrix
     id-loc ;;map from node id to location tuple
     noisy-locs ;;locations that generates false positive using long range sensor
     tree
     ]
  ActiveClass
  (hypotheses [this] (set (keys survivor-priors)))

  (locations [this] (set (range (count id-loc))))

  (priors [this] survivor-priors)

  ;; take a random float between 0 to 1
  ;; 0.9 noisy
  (obs [this h loc randnum]
    (let [obs-table (obs-prob this h loc)]
      (loop [entries obs-table
             cum-prob 0]
        (let [[obs prob] (first entries)
              ]
          (if (>= (+ cum-prob prob) randnum)
            obs
            (recur (rest entries) (+ cum-prob prob)))))))

  ;; returns a map of obs to probability
  (obs-prob [this h loc]
    (let [[x y z] (id-loc loc)]
      (if (not= z 0)
        ;; long range observation
        (if (some true? (map (fn [oloc]
                            (and (= oloc h) (not (contains? noisy-locs oloc))))
                          (map #(map + %1 %2) (repeat [x y])
                               (for [dx [1 0 -1]
                                     dy [1 0 -1]]
                                 [dx dy]))))
          {1 (- 1 noisy-level) 0 noisy-level}
          {0 (- 1 noisy-level) 1 noisy-level}
          )
        ;; true or false and special obs
        (if (= [x y] h)
          {true 1.0 false 0.0}
          {false 1.0 true 0.0})
        )))

  (metric [this] distance-matrix)

  (metric-tree [this] @tree)

  (split [this new-h-set]
    (assoc this :survivor-priors new-h-set))
  )

(defn make-rescue-problem-noisy
  ([size noisy-locs dist-h dist-stretch]
     (let [[distance-matrix id-loc] (make-grid-metric size size dist-h dist-stretch)]
       (->RescueProblemNoisy
        ;; location
        (into {} (for [x (range size)
                       y (range size)]
                   [[x y] (/ 1 (* size size))]))
        distance-matrix
        id-loc
        noisy-locs
        (delay (graph-partition-derand distance-matrix))
        ))
     )

  )
