(ns domains.grasping-model
  (:require ;;[incanter.charts :refer [scatter-plot]]
            [incanter.core :refer :all]
            ))

(defprotocol Shape
  (draw [this] "return points to draw raster on workspace")
  (translate [this x y] "translate shape")
  (rotate [this angle] "rotate shape")
  (min-point [this] "return the mininum x and minimum y coordinate")
  (laser-intersection [this laser-pos angle])
  )

(defn- rotate-point [point angle]
  (let [theta (/ (* angle Math/PI) 180)
        cos-theta (Math/cos theta)
        sin-theta (Math/sin theta)
        [x y] point]
    (vec (map #(Math/round %) [(- (* cos-theta x) (* sin-theta y))
                               (+ (* sin-theta x) (* cos-theta y))
                               ]))))

(defn point-on-segment [pt x0 y0 x1 y1]
  (let [[xi yi] pt
            left  (min x0 x1)
            right (max x0 x1)
            top (min y0 y1)
            bottom (max y0 y1)]
        (if (and (>= xi left) (<= xi right) (>= yi top) (<= yi bottom))
          [(Math/round xi) (Math/round yi)]
          nil)))

;; given two lines in form Ax + By = C
(defn lines-intersection [[A1 B1 C1] [A2 B2 C2]]
  (let [det (- (* A1 B2) (* A2 B1))]
    (if (zero? det)
      nil
      [(/ (- (* B2 C1) (* B1 C2)) det)
       (/ (- (* A1 C2) (* A2 C1)) det)])))


(defn line-circle-intersection [x0 y0 x1 y1 cx cy r]
  (let [dx (- x1 x0)
        dy (- y1 y0)
        dr (Math/hypot dx dy)
        D (- (* (- x0 cx) (- y1 cy)) (* (- x1 cx) (- y0 cy)))
        discriminant (- (* r r dr dr) (* D D))]
    (if (> discriminant 0)
      (let [Ddy (* D dy)
            sqrt-disc (Math/sqrt discriminant)
            root-x (* (Math/signum (float dy)) dx sqrt-disc)
            Ddx (- (* D dx))
            root-y (* (Math/abs dy) sqrt-disc)
            idr2 (/ 1 (* dr dr))
            ]
        (map #(->>
               %
               (map (partial * idr2))
               (map + [cx cy])
               (map (fn [a] (Math/round (float a)))))
             [[(+ Ddy root-x)  (- root-y Ddx)]
                [(- Ddy root-x)  (- 0  root-y Ddx)]]
             )
        )))
  )

(defrecord Circle [x0 y0 radius]
  Shape
  (draw [this]
    (loop [x 0 y radius m (- 5 (* 4 radius)) points '()]
      (let [x+ (+ x0 x)
            x- (- x0 x)
            y+ (+ y0 y)
            y- (- y0 y)
            x0y+ (+ x0 y)
            x0y- (- x0 y)
            xy0+ (+ y0 x)
            xy0- (- y0 x)
            new-points [[x+ y+] [x+ y-] [x- y+] [x- y-]
                        [x0y+ xy0+] [x0y+ xy0-] [x0y- xy0+] [x0y- xy0-]]
            [y m] (if (> m 0)
                    (vector (dec y) (- m (* 8 y)))
                    (vector y m))]
        (if (<= x y)
          (recur (inc x)
                 y
                 (+ m 4 (* 8 x))
                 (concat points new-points))
          points))))

  (translate [this x y]
    (->Circle (+ x0 x) (+ y0 y) radius))

  (rotate [this angle]
    (let [[new-x0 new-y0] (rotate-point [x0 y0] angle)]
      (->Circle new-x0 new-y0 radius))
    )

  (min-point [this] [(- x0 radius) (- y0 radius)])

  (laser-intersection [this [x y] angle]
    (let [dir (if (>= angle 180) -1 1)
          x1 (+ x dir)
          y1 (+ y (* dir (Math/tan (Math/toRadians angle))))]
      (line-circle-intersection x y x1 y1 x0 y0 radius)))
  )

(defrecord Line [x0 y0 x1 y1]
  Shape
  (draw [this]
    "(Credit rosettacode.org) Draw a line from x1,y1 to x2,y2 using Bresenham's"
    (let [dist-x (Math/abs (- x0 x1))
          dist-y (Math/abs (- y0 y1))
          steep (> dist-y dist-x)]
      (let [[x0 y0 x1 y1] (if steep [y0 x0 y1 x1] [x0 y0 x1 y1])]
        (let [[x0 y0 x1 y1] (if (> x0 x1) [x1 y1 x0 y0] [x0 y0 x1 y1])]
          (let  [delta-x (- x1 x0)
                 delta-y (Math/abs (- y0 y1))
                 y-step (if (< y0 y1) 1 -1)]
            (let [point (if steep
                          #(vector (int %2) (int %1))
                          #(vector (int %1) (int %2))
                          )]
              (loop [x x0 y y0 error (Math/floor (/ delta-x 2)) points '()]
                (if (< x x1)
                                        ; Rather then rebind error, test that it is less than delta-y rather than zero
                  (if (< error delta-y)
                    (recur (inc x) (+ y y-step) (+ error (- delta-x delta-y))
                           (conj points (point x y)))
                    (recur (inc x) y            (- error delta-y)
                           (conj points (point x y))))
                  points))))))))

  (translate [this x y]
    (->Line (+ x0 x) (+ y0 y) (+ x1 x) (+ y1 y)))

  (rotate [this angle]
    (apply ->Line (flatten (map #(rotate-point % angle) [[x0 y0] [x1 y1]])))
    )

  (min-point [this] [(min x0 x1) (min y0 y1)])

  (laser-intersection [this [x y] angle]
    (let [A1 (- y1 y0)
          B1 (- x0 x1)
          C1 (+ (* A1 x0) (* B1 y0))
          A2 (Math/tan (Math/toRadians angle))
          B2 -1
          C2 (+ (* A2 x) (* B2 y))
          pt (lines-intersection [A1 B1 C1] [A2 B2 C2])
          ]
      (if pt
        [(point-on-segment pt x0 y0 x1 y1)]
        nil)
      ))
  )

(defrecord VectorModel [vectors points dim])

(defn- translate-points [points x y]
  (for [[x0 y0] points]
    [(+ x0 x) (+ y0 y)]))

(defn make-vector-model [vectors]
  (let [points (apply concat (map draw vectors))]
    (->VectorModel
     vectors
     points
     [(apply max (map first points))
      (apply max (map second points))])))

;; rotate a model by angle (in degree)
(defn rotate-model [model angle [cx cy]]
  (make-vector-model (->> (:vectors model)
                          (map #(translate % (- cx) (- cy)))
                          (map #(rotate % angle))
                          (map #(translate % cx cy))))
  )

(defn translate-model [model x y]
  (let [tran-vectors (map #(translate % x y) (:vectors model))]
    (make-vector-model tran-vectors)))

(comment (defn plot-model [model]
           (let [[x y] (apply map vector (:points model))]
             (view (scatter-plot x y)))
           ))

(defn concat-model [models]
  (make-vector-model (reduce concat (map #(:vectors %) models)))
  )

(def cup-with-handle
  (make-vector-model [(->Circle 20 20 20)
                      (->Line 50 18 55 18)
                      (->Line 50 18 50 22)
                      (->Line 50 22 55 22)
                      (->Line 55 18 55 22)]
                     ))

(def smaller-cup
  (make-vector-model [(->Circle 15 15 15)]))

(def cup-no-handle
  (make-vector-model [(->Circle 20 20 20)])
  )
