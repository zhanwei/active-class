(ns domains.grid-search-expt
  (:require [active-class.problem :refer :all]
            [active-class.simulator :refer :all]
            [domains.grid-search :refer :all]
            [active-class.isolation-alg :refer [iso-alg]]
            [experiment.runner :refer :all]
            [experiment.baseline :refer :all]
            [clojure.math.numeric-tower :refer [sqrt]]
            )
  )

(comment (def noisy-locs #{[2 2] [5 5] [1 1] [4 5] [5 6] [3 5] [5 7] [0 2] [2 5] [0 4] [1 5] [0 5] [7 2] [5 2] [3 1] [4 2]}))

(comment (def trails [[[0 2] [0 3] [0 4] [0 4] [1 5] [2 6] [2 7]] [[2 7] [2 6] [2 6] [3 5] [3 4] [3 3] [3 3] [4 2] [5 2]] [[5 2] [4 2] [4 2] [4 3] [4 3] [4 4] [4 5] [4 6] [4 6] [4 7]]]))

(def trails [])

(def loc-positive {[4 3] false, [3 3] false, [4 4] true, [3 4] false, [4 5] false, [3 5] false, [4 6] true, [0 2] false, [4 7] false, [0 3] false, [2 6] true, [0 4] false, [1 5] false, [2 7] true, [5 2] true, [4 2] true})

;; noisy locs derived from
(def noisy-locs #{[3 3] [4 3] [5 3] [3 4] [4 4] [5 4] [3 5][4 5] [5 5] [6 3] [7 3] [6 4] [7 4] [6 5] [7 5]})

(def problem (assoc (make-rescue-problem 8 trails {} noisy-locs 10 4)
               :tree (delay (read-string (slurp "new-grid-search-graph.dat"))))
  )

(def test-small-problem (make-rescue-problem 3 0 10 4))

(defn gen-snake-path [size]
  (loop [xs (range size)
         path []
         direction 1]
    (if-let [x (first xs)]
      (recur
       (rest xs)
       (loop [ys (if (= direction 1)
                   (range size)
                   (range (dec size) -1 -1))
              path2 path]
         (if-let [y (first ys)]
           (recur (rest ys) (conj path2 [x y]))
           path2))
       (* direction -1)
       )
      path)))

;; go to the next location, so that it will cover all location with
;; lowest cost follows a simple rule: on even rows move right, odd
;; rows move left. If at the end of row, move down. If reach end of
;; map, move to [0 0]
;; move to lower level if it is not already there
(defn visit-all-next [problem curr-loc history]
  (let [size (sqrt (/ (count (locations problem)) 2))
        [x y z] ((:id-loc problem) curr-loc)
        ;;_ (println x y z)
        ]
    (.indexOf (:id-loc problem)
              (cond (= z 1)
                    [x y 0]
                    ;; wrap back to [0 0 0]
                    (= [x y z] (if (even? (dec size))
                                 [(dec size) (dec size) 0]
                                 [0 (dec size) 0]))
                    [0 0 0]
                    :else
                    (if (even? y)
                      ;; move right with wrap around
                      (if (< (inc x) size)
                        [(inc x) y 0]
                        [x (inc y) 0])
                      ;; move left with wrap around
                      (if (>= (dec x) 0)
                        [(dec x) y 0]
                        [x (inc y) 0]))))
    ))


(defn visit-all-alg [instance simulator]
  (env-loop instance simulator visit-all-next))


(defn output-tikz-trace [trace]
  (let [coords (map #(if (not= 0 (% 2))
                       (str "top-" (% 0) "-" (% 1))
                       (str "bottom-" (% 0) "-" (% 1)))
                    trace)]
    (do
      (print "\\draw ")
      (doseq [c coords]
        (print (str "(" c  ")") " -- " )
        ))))


(defn output-tikz-noisy-loc [noisy-locs]
  (do
    (doseq [l noisy-locs]
      (print (str (first l) "/" (second l) ) ", " )
      )))
