(defproject active_class "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/math.numeric-tower "0.0.2"]
                 ;; [org.clojure/core.typed "0.2.13"]
                 [org.clojure/math.combinatorics "0.0.7"]
                 [incanter "1.5.1"]
                 [org.jordanlewis/data.union-find "0.1.0"]
                 ;; [quil "1.6.0"]
                 [org.clojure/tools.trace "0.7.6"]]
  :plugins [[lein-git-deps "0.0.1-SNAPSHOT"]
            ;; [lein-typed "0.3.1"]
            [lein-kibit "0.0.8"]]
  :git-dependencies [["https://github.com/aysylu/loom.git"]]
  :source-paths ["src" ".lein-git-deps/loom/src/"]
  ;:main domains.noisy.grid-search-expt
  :main experiment.command-centre
  )
