(ns metric-embedding.embed-test
  (:require [loom.graph :refer :all]
            [loom.gen :refer :all]
            [clojure.test :refer :all]
            [metric-embedding.embed :refer :all]
            ))

(def mygraph  #loom.graph.BasicEditableWeightedGraph{:nodeset #{0 1 2 3 4 5 6 7 8 9}, :adj {0 {9 73, 7 80, 5 60, 4 26}, 1 {5 94, 4 76}, 2 {3 35, 4 67}, 3 {6 58, 2 35}, 4 {7 79, 2 67, 9 84, 6 44, 0 26, 1 76, 5 2}, 5 {1 94, 7 52, 0 60, 8 44, 4 2}, 6 {8 25, 7 65, 3 58, 4 44}, 7 {6 65, 5 52, 4 79, 9 46, 0 80}, 8 {6 25, 9 10, 5 44}, 9 {0 73, 7 46, 4 84, 8 10}}})

(deftest test-graph-partition
  (let [graph (:tree (read-string (slurp "test_case1.txt")))]
    (is (= nil
           (graph-partition ))))
  )
