(ns experiment.baseline-test
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [clojure.test :refer :all]
            [experiment.baseline :refer :all]
            [active-class.simulator :refer :all]
            [domains.grid-search :refer :all]
            )
  )

(deftest test-entropy
  (let [problem (read-string (slurp "grid_search_problem.txt"))]
    (is (= 4.158883083359677 (entropy problem)))))

(deftest test-info-gain
  (let [problem (read-string (slurp "grid_search_problem.txt"))]
    (is (= 0 (info-gain problem [1 2 3 4 5])))))
