(ns active-class.group-steiner-test
  (:require [clojure.test :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer :all]
            [active-class.group-steiner :refer :all]
            [active-class.tree :refer :all]))

(def wdg (weighted-graph {:a {:c 19 :d 20} :d {:e 15 :f 10}}))

(def wdg2 (digraph { :d [:e :f]}))

(def gs1 (->GroupSteiner wdg [#{:a} #{:e :d} #{:f :c}] :a))

(deftest test-is-subgraph
  (is (is-subgraph? wdg2 wdg)))

(deftest test-num-covered
  (is (= (num-groups-covered gs1 wdg2) 2)))

(deftest test-tree-weight
  (is (= 25 (tree-weight gs1 wdg2))))
