(ns active-class.version_sp_fn_test
  (:require [clojure.test :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer :all]
            [active-class.version-sp-fn :refer :all]
            [active-class.problem :refer :all]
            [domains.grasping-expt :refer :all]
            [active-class.submodular-fn :refer :all]
))

(def version-sp1 (make-version-sp-fn problem))

(deftest test-profits
  (is (= 1.0 (profits version-sp1 (locations problem))))
  )

(deftest test-min-profit
  (is (= 0.01531944444444433 (min-profit version-sp1)))
  )

(deftest test-update-fn
  (is (= 1.0 (+ (profits (update-fn version-sp1 (take 1 (locations problem)))
                       (locations problem))
                (profits version-sp1 (take 1 (locations problem))))))
  )
