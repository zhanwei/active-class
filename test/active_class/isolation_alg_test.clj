(ns active-class.isolation-alg-test
  (:require [active-class.problem :refer :all]
            [active-class.isolation-alg :refer :all]
            [active-class.simulator :refer :all]
            [active-class.problem-test :refer [test-problem]]
            [clojure.test :refer :all]
            )
  )
(deftest test-partition-by-loc
  (is (= (partition-by-loc test-problem 1) {[1 2] [:a :c :d :e] [5 2] [:b]}))
  )

(deftest test-partition-by-loc2
  (is (= (partition-by-loc test-problem 4) nil ))
  )

(deftest test-big-small-sets
  (is (= (big-small-sets test-problem (partition-by-loc test-problem 4))
         ['() '(:a :c :b :d :e) '((6) (7) (5) (9))]))
  )

(deftest test-partition-alg
  (is (= (partition-alg test-problem 0) nil)))


(deftest test-isolation-alg
  (is (= [:e 0] (iso-alg test-problem
                         (make-noiseless-simulator test-problem :e 0)))))
