(ns active-class.problem-test
  (:require [loom.graph :refer :all]
            [active-class.problem :refer :all]
            [clojure.test :refer :all]
            )
  )

(def test-problem (->ActiveClassNoiseless
                   ;; hypotheses
                   ;;;; 0 1 2 3 4 5
                   {:a [1 2 3 4 5 6]
                    :b [5 2 3 4 5 5]
                    :c [1 2 3 7 7 7]
                    :d [1 2 7 7 8 9]
                    :e [1 2 9 9 9 9]}
                   ;;location-obs
                   {0  [0 1]
                    1  [0 2 3 5]
                    2  [4 5]
                    3  [5]
                    4  [0]
                    5  [1]}
                   ;;prior
                   {:a 0.2, :c 0.2, :b 0.2, :d 0.2, :e 0.2}
                   ;;metric
                   #loom.graph.BasicEditableWeightedGraph{:nodeset #{0
                   1 2 3 4 5}, :adj {4 {3 44}, 1 {3 47, 2 30, 0 34}, 0
                   {2 13, 1 34}, 3 {4 44, 1 47, 5 3}, 2 {0 13, 1 30, 5
                   91}, 5 {3 3, 2 91}}} ))

(deftest test-obs
  (is (= (obs test-problem :b 1 nil) [5 2]))
  )


(comment


  (deftest test-partition-by-path
    (is (= (h-partition-by-path test-problem [1 2]) {1 {[1 2] [:a :c :d :e] [5 2] [:b]}}))
    ))
