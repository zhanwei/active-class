(ns active-class.group-steiner-tree-test
  (:require [clojure.test :refer :all]
            [loom.graph :refer :all]
            [clojure.set :refer :all]
            [active-class.group-steiner :refer :all]
            [active-class.group-steiner-tree :refer :all]))

(def wdg (weighted-digraph [:a :b 20] [:a :c 4] [:b :d 3] [:b :f 9] [:c :g 10]
                           [:c :h 20] [:h :i 4]))

(def wdg2 (digraph { :d [:e :f]}))

(def gst (->GroupSteinerTree wdg [#{:f} #{:g}] :a))

(deftest test-eliminate-non-terminal-leaf
  (is (= (:tree  (eliminate-non-terminal-leaf gst))
         #loom.graph.BasicEditableWeightedDigraph{:nodeset #{:a :c :b :f :g},
                                                  :adj {:c {:g 10}, :b {:f 9},
                                                        :a {:c 4, :b 20}}, :in
                                                  {:g #{:c}, :f #{:b}, :c #{:a}, :b #{:a}}}
         ))
  )

(def sometree (weighted-digraph [:a :b 20] [:a :c 4] [:b :d 3] [:b :f 9] [:c :g 10]
                           [:c :h 20] [:h :i 4] [:i :j 5]))

(def gsometree (->GroupSteinerTree sometree [#{:f} #{:g}] :a))

(deftest test-eliminate-d2-interior
  (is (= (:tree (eliminate-degree-two-interior gst2))
         #loom.graph.BasicEditableWeightedDigraph{:nodeset #{:a :c :b :f :g :d :j}, :adj {:c {:j 29, :g 10}, :b {:f 9, :d 3}, :a {:c 4, :b 20}}, :in {:j #{:c}, :g #{:c}, :f #{:b}, :d #{:b}, :c #{:a}, :b #{:a}}})))
