(ns active-class.noisy.edge-cutting-isolation-test
  (:require [clojure.test :refer :all]
            [active-class.problem :refer :all]
            [active-class.noisy.edge-cutting-isolation :refer :all]
            [domains.noisy.grid-search :refer :all]
            )
  )


;; noisy locs derived from
(def noisy-locs #{[3 3] [4 3] [5 3] [3 4] [4 4] [5 4] [3 5][4 5] [5 5] [6 3] [7 3] [6 4] [7 4] [6 5] [7 5]})

(def problem (assoc (make-rescue-problem-noisy 8 noisy-locs 10 4)
               :tree (delay (read-string (slurp "new-grid-search-graph.dat")))))

;; test sampling from uniform distribution
(deftest test-sampler-uniform
  (let [sampler (make-hypothesis-sampler (hypotheses problem) (priors problem))]
    (is (= (vec (hypotheses problem))
           (for [i (range 64)
                 :let [r (+ (* i (/  1 64)) 0.001)]]
             (sampler r nil))))))

(deftest test-sampler-non-uniform
  (let [hs (vec (hypotheses problem))
        num-hypotheses (count hs)
        non-uniform-prior (-> (reduce #(assoc %1 %2 0) (priors problem)
                                      (take (/ num-hypotheses 2) hs))
                              ((fn [p]
                                 (reduce #(update-in %1 [%2] (partial * 2)) p
                                         (drop (/ num-hypotheses 2) hs)))))
        sampler (make-hypothesis-sampler (hypotheses problem) non-uniform-prior)
        ]
    (is (= (->> hs
                (drop (/ num-hypotheses 2))
                (map #(repeat 2 %))
                (apply concat)
                )
           (for [i (range 64)
                 :let [r (+ (* i (/  1 64)) 0.001)]]
             (sampler r nil))))))

(deftest test-sample-wo-replacement
  (let [sampler (make-hypothesis-sampler (hypotheses problem) (priors problem))
        h1 (sampler 0.5 nil)
        h2 (sampler 0.5 h1)
        _ (println h1 h2)]
    (is (not= h1 h2))
    ))

(deftest test-make-EC-model
  (let [test-model (make-edge-cutting-model problem 20 (locations problem))]
    (is (= nil test-model)))
  )
