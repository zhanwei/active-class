(ns active-class.noisy.edge-cut-fn-test
  (:require [clojure.test :refer :all]
            [active-class.problem :refer :all]
            [active-class.submodular-fn :refer :all]
            [active-class.noisy.edge-cut-fn :refer :all]
            [domains.noisy.grid-search :refer :all]))

;; noisy locs derived from
(def noisy-locs #{[3 3] [4 3] [5 3] [3 4] [4 4] [5 4] [3 5][4 5] [5 5] [6 3] [7 3] [6 4] [7 4] [6 5] [7 5]})

(def problem (assoc (make-rescue-problem-noisy 8 noisy-locs 10 4)
               :tree (delay (read-string (slurp "new-grid-search-graph.dat")))))


(def myfn (make-edge-cut-fn problem (locations problem)))

(deftest test-profit
  (is (= 0.015380859375 (profits myfn [(first (locations problem))])))
  (is (= 0.1001285156250028 (profits myfn [((locations problem) 67)])))
  )

(deftest test-update-fn
  (let [updated-fn (update-fn myfn [(first (locations problem))])
        updated-fn-2 (update-fn myfn [((locations problem) 67)])
        updated-fn-3 (update-fn myfn [((locations problem) 68)])]
    (is (= 0.0 (profits updated-fn [(first (locations problem))])))
   ( is (= (+ (profits updated-fn [(second (locations problem))])
               (profits myfn [(first (locations problem))]))
           (profits myfn (take 2 (locations problem)))))

    (is (= 0.0 (profits updated-fn-2 [((locations problem) 67)])))
    (is (= (+ (profits updated-fn-2 [68])
              (profits myfn [67]))
           (+ (profits updated-fn-3 [67])
              (profits myfn [68]))
           (profits myfn [68 67]))

        )))

(deftest test-update-other
  (let [updated-fn (update-fn myfn [(first (locations problem))])
        updated-other (update-other-obs myfn (first (locations problem))
                                        (do (comment (first ((:o_max_prob myfn)
                                                     (first (locations problem)))))
                                            true))
        ]
    (is (= (:total-profit updated-fn) (:total-profit updated-other) ))
    (is (= (profits updated-fn [(second (locations problem))])
           (profits updated-other [(second (locations problem))])))
  )
)
