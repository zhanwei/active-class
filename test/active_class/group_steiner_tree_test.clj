(ns active-class.group-steiner-tree-test
  (:require [clojure.test :refer :all]
            [clojure.set :refer :all]
            [tree.tree :refer :all]
            [loom.graph :refer [nodes]]
            [active-class.group-steiner-tree :refer :all]))

(def tree (convert-to-graph (make-zip-tree [[[[8] [9] 4] [[10] [11] 5] 2]
                                           [[[12] [13] 6] [[14] [15] 7] 3] 1])))

(def groups {0 #{4 8 9} 1 #{11} 2 #{12 10}})

(deftest test-eliminate-non-terminal-leaf
  (is (= (eliminate-non-terminal-leaf tree groups)
         (convert-to-graph (make-zip-tree [[[[8] [9] 4] [[10] [11] 5] 2] [[[12] 6] 3] 1]))
         ))
  )

(comment 5 - 4
               \
                2 ---
               /      \
              6        \
                        1
              10       /
                \     /
                 3 ---
                /
       9 - 8 - 7
)

(def weights2 (into {} (map #(vector % 5) (range 1 11))))

(def tree2 (convert-to-graph (make-zip-tree  [[[[5] 4] [6] 2] [[[[9] 8] 7] [10] 3] 1])
                             (into {} (map #(vector % 5) (range 1 11)))))

(def groups2 {0 #{4 5} 1 #{6 9 8} 2 #{3}})

(deftest test-eliminate-d2-interior
  (is (= (eliminate-degree-two-interior
          tree2
          groups2)
         (convert-to-graph (make-zip-tree [[[[5] 4] [6] 2] [[[9] 8] [10] 3] 1])
                           {5 5 4 5 6 5 2 5 9 5 8 10 10 5 3 5 1 5} )))
  )

(deftest test-add-dummy-group
  (is (= (add-dummy-group (nodes tree) groups)
         {0 #{4 8 9}, 1 #{11}, 2 #{1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}}
         )))
