(ns active-class.gs-greedy-alg-test
  (:require [clojure.test :refer :all]
            [clojure.set :refer :all]
            [tree.tree :refer :all]
            [loom.graph :refer [nodes edges weight successors]]
            [active-class.gs-greedy-alg :refer :all]
            [active-class.group-steiner-tree :refer :all]
            ))

(comment (def test-gst (gen-rand-gs-tree 5 100 5 10 4)))

(def hr-root 124)

(def test-hr-tree
  #loom.graph.BasicEditableWeightedDigraph{:nodeset #{96 128 160 97 129 161 98 130 162 99 131 163 100 132 164 101 133 165 102 134 166 103 135 167 72 104 136 168 73 105 137 169 74 106 138 170 75 107 139 171 76 108 140 172 77 109 141 173 78 110 142 174 79 111 143 80 112 144 81 113 145 82 114 146 83 115 147 84 116 148 85 117 149 86 118 150 87 119 151 88 120 152 89 121 153 90 122 154 91 123 155 92 124 156 93 125 157 94 126 158 95 127 159}, :adj {98 {118 1.0, 123 1.0}, 99 {169 2.0, 173 2.0, 87 2.0, 148 2.0, 97 3.0, 116 3.0, 98 2.0, 115 2.0}, 131 {151 2.0, 110 2.0, 140 2.0, 76 2.0, 127 2.0, 117 2.0}, 100 {126 1, 129 1}, 101 {153 1.0, 137 1.0, 142 1.0, 161 1.0}, 102 {139 1.0, 132 1.0}, 168 {80 1, 106 1, 105 1}, 169 {93 1.0, 168 1.0, 84 1.0, 83 1.0, 79 1.0}, 138 {160 1, 81 1, 82 1}, 76 {92 1.0, 86 1.0}, 140 {144 1.0, 85 1.0}, 173 {75 1.0, 155 1.0}, 110 {111 1.0, 156 1.0, 133 1.0}, 111 {164 1, 141 1}, 145 {157 1, 78 1, 77 1}, 115 {107 1.0, 108 1.0, 122 1.0}, 147 {154 1, 162 1, 174 1}, 84 {104 1, 120 1, 121 1}, 148 {138 1.0, 91 1.0, 94 1.0, 90 1.0}, 117 {109 2.0, 74 2.0}, 149 {159 1, 167 1, 166 1, 146 1, 143 1}, 118 {95 1, 113 1}, 150 {102 2.0, 101 2.0, 128 3.0, 96 3.0}, 87 {147 1.0, 72 1.0, 152 1.0, 73 1.0}, 151 {149 1.0, 158 1.0, 145 1.0, 134 1.0, 136 1.0}, 92 {163 1, 165 1}, 124 {125 8.0, 130 15.0}, 93 {103 1, 112 1, 114 1, 89 1, 88 1}, 125 {99 4.0, 131 4.0, 150 4.0, 172 7.0, 119 7.0}, 94 {171 1, 170 1}, 127 {100 1.0, 135 1.0}}, :in {96 #{150}, 128 #{150}, 160 #{138}, 97 #{99}, 129 #{100}, 161 #{101}, 98 #{99}, 130 #{124}, 162 #{147}, 99 #{125}, 131 #{125}, 163 #{92}, 100 #{127}, 132 #{102}, 164 #{111}, 101 #{150}, 133 #{110}, 165 #{92}, 102 #{150}, 134 #{151}, 166 #{149}, 103 #{93}, 135 #{127}, 167 #{149}, 72 #{87}, 104 #{84}, 136 #{151}, 168 #{169}, 73 #{87}, 105 #{168}, 137 #{101}, 169 #{99}, 74 #{117}, 106 #{168}, 138 #{148}, 170 #{94}, 75 #{173}, 107 #{115}, 139 #{102}, 171 #{94}, 76 #{131}, 108 #{115}, 140 #{131}, 172 #{125}, 77 #{145}, 109 #{117}, 141 #{111}, 173 #{99}, 78 #{145}, 110 #{131}, 142 #{101}, 174 #{147}, 79 #{169}, 111 #{110}, 143 #{149}, 80 #{168}, 112 #{93}, 144 #{140}, 81 #{138}, 113 #{118}, 145 #{151}, 82 #{138}, 114 #{93}, 146 #{149}, 83 #{169}, 115 #{99}, 147 #{87}, 84 #{169}, 116 #{99}, 148 #{99}, 85 #{140}, 117 #{131}, 149 #{151}, 86 #{76}, 118 #{98}, 150 #{125}, 87 #{99}, 119 #{125}, 151 #{131}, 88 #{93}, 120 #{84}, 152 #{87}, 89 #{93}, 121 #{84}, 153 #{101}, 90 #{148}, 122 #{115}, 154 #{147}, 91 #{148}, 123 #{98}, 155 #{173}, 92 #{76}, 156 #{110}, 93 #{169}, 125 #{124}, 157 #{145}, 94 #{148}, 126 #{100}, 158 #{151}, 95 #{118}, 127 #{131}, 159 #{149}}}
  )


(comment
  (def test-gst #active_class.group_steiner_tree.GroupSteinerTree{:tree #loom.graph.BasicEditableWeightedDigraph{:nodeset #{0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22}, :adj {2 {3 43, 4 66, 5 79, 6 88, 7 82}, 1 {2 55}, 10 {11 64, 12 13, 13 38}, 15 {16 99}, 9 {10 94, 14 77, 15 52}, 18 {19 25}, 20 {21 49, 22 2}, 17 {18 40, 20 67}, 0 {1 100, 8 56, 9 96, 17 58}}, :in {1 #{0}, 2 #{1}, 3 #{2}, 4 #{2}, 5 #{2}, 6 #{2}, 7 #{2}, 8 #{0}, 9 #{0}, 10 #{9}, 11 #{10}, 12 #{10}, 13 #{10}, 14 #{9}, 15 #{9}, 16 #{15}, 17 #{0}, 18 #{17}, 19 #{18}, 20 #{17}, 21 #{20}, 22 #{20}}}, :groups {1 #{8 13 16 19 20}, 2 #{11 14 17 21}, 3 #{5 8 9 11 19 21 22}, 4 #{11 14 21}}, :profits {1 3.415125605245788, 2 7.012810412634752, 3 6.206154145116365, 4 0.7412292053682912}, :root 0}
    ))

;;; testing with profits that adds up to one
(def test-gst (->GroupSteinerTree

               #loom.graph.BasicEditableWeightedDigraph{:nodeset #{0 [0 1 2] 1 2 3 [3 5] [0 2] 4 [0 1 2 3 4 5] 5}, :adj {[0 2] {0 15.0, 2 15.0}, [0 1 2] {[0 2] 16.0, 1 31.0}, [3 5] {3 31.0, 5 31.0}, [0 1 2 3 4 5] {4 63.0, [0 1 2] 32.0, [3 5] 32.0}}, :in {4 #{[0 1 2 3 4 5]}, 0 #{[0 2]}, 2 #{[0 2]}, [0 2] #{[0 1 2]}, 1 #{[0 1 2]}, [0 1 2] #{[0 1 2 3 4 5]}, 3 #{[3 5]}, 5 #{[3 5]}, [3 5] #{[0 1 2 3 4 5]}}}
               {:e #{2 3 4}, :d #{2 3 4}, :c #{2 3 4}, :a #{2 3 4}, :b #{1 2 3 4 5}}
               {:a 0.2, :c 0.2, :b 0.2, :d 0.2, :e 0.2}
               [0 1 2 3 4 5]))



(comment
  (deftest test-total-profits
  (is (= (apply + (vals (:profits test-gst)))
         (total-profits (nodes (:tree test-gst)) (:groups test-gst) (:profits test-gst)))))

  (deftest test-augmenting
    (is (= #active_class.gs_greedy_alg.TreeCover{:nodes #{0 1 2 3}, :weight 209, :covered #{2 3 4}}
           (augment-cover (->TreeCover #{0 1 2}
                                       120
                                       #{2 3})
                          (->TreeCover #{2 3}
                                       100
                                       #{4})
                          (:tree test-gst)
                          (:groups test-gst))))
    ))

(defn cal-profits [gst cover]
  (profits-covered (:profits gst)
                   (:covered cover)))

(deftest test-big-instance
  (let [big-gst (read-string (slurp "debug-instance.txt"))]
    (is (>= (cal-profits big-gst (time (greedy-gs big-gst (/ 33 64) 0))) (/ 33 64)))))

(deftest test-assertion-failure
  (let [big-gst (read-string (slurp "test_case1.txt"))
        target (+ 0.5 (apply min (vals (:profits big-gst))))]
    (is (>= (cal-profits big-gst (time (greedy-gs big-gst target 0))) target))))

(deftest test-covering
  (let [total (apply + (vals (:profits test-gst)))]
    (is (>= (cal-profits test-gst (greedy-gs test-gst (/ total 3) 0))
           (/ total 3)))
    (is (>= (cal-profits test-gst (greedy-gs test-gst (/ total 2) 0))
           (/ total 2)))
    (is (>= (cal-profits test-gst (greedy-gs test-gst total 0))
           total))
    )
  )

(comment
 (deftest test-max-cover
   (let [total (apply + (vals (:profits test-gst)))
         total-weight (apply + (map
                                (partial apply weight (:tree test-gst))
                                (edges (:tree test-gst))))
         _ (println "total" total)
         _ (println "total weight" total-weight)]
     (is (>= (cal-profits test-gst (greedy-gs test-gst total total-weight))
             total))
     (is (let [result (greedy-gs test-gst (/ total 2) (/ total-weight 10))
               _ (println result)]
           (and (<= (:weight result) (/ total-weight 10))
                (= (:weight (greedy-gs test-gst (cal-profits test-gst result) 0))
                   (:weight result))))))
   )
