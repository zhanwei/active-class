(ns domains.grid-search-test
  (:require [active-class.problem :refer :all]
            [loom.graph :refer :all]
            [active-class.isolation-alg :refer [iso-alg partition-alg]]
            [active-class.simulator :refer :all]
            [clojure.test :refer :all]
            [domains.grid-search :refer :all]))


(def test-noisy-locs #{[2 2] [5 5] [1 1] [4 5] [5 6] [3 5] [5 7] [0 2] [2 5] [0 4] [1 5] [0 5] [7 2] [5 2] [3 1] [4 2]})

(def test-trails [[[0 2] [0 3] [0 4] [0 4] [1 5] [2 6] [2 7]] [[2 7] [2 6] [2 6] [3 5] [3 4] [3 3] [3 3] [4 2] [5 2]] [[5 2] [4 2] [4 2] [4 3] [4 3] [4 4] [4 5] [4 6] [4 6] [4 7]]])

(def test-loc-positive {[4 3] false, [3 3] false, [4 4] true, [3 4] false, [4 5] false, [3 5] false, [4 6] true, [0 2] false, [4 7] false, [0 3] false, [2 6] true, [0 4] false, [1 5] false, [2 7] true, [5 2] true, [4 2] true})

(def test-problem (make-rescue-problem 8 test-trails test-loc-positive test-noisy-locs))

(def test-small-problem (make-rescue-problem 2 0))

(def sim-small (make-noiseless-simulator test-small-problem  [1 1] 1))
(def sim (make-noiseless-simulator test-problem  [1 3] 10))

(deftest test-small-isolation
  (is (= (partition-alg test-small-problem 3)))
  (is (= [1 1] (first (time (iso-alg test-small-problem sim-small))))))

(deftest test-isolation
  (let [sim (make-noiseless-simulator test-problem [1 3] 10)]
    (is (= [1 3] (first (time (iso-alg test-problem sim)))))))
